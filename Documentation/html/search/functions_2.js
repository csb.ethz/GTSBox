var searchData=
[
  ['generatedummyvar_204',['generateDummyVar',['../d5/ddd/generate_dummy_var_8m.html#aa8b4264401f69256e730ceee4bcb7bb9',1,'generateDummyVar.m']]],
  ['get_5far_5fgm_205',['get_AR_gm',['../d2/dee/get___a_r__gm_8m.html#ae4ac1585a9f9586bc9cd3c568c87b6f0',1,'get_AR_gm.m']]],
  ['get_5fpl_5fgm_206',['get_PL_gm',['../d4/dc9/get___p_l__gm_8m.html#aa13fb8d33e0707ddf898900de6171cd9',1,'get_PL_gm.m']]],
  ['getai_5fdefault_207',['getAi_default',['../d9/d2e/get_ai__default_8m.html#ab9cf1c01b7c061985281d4406e7a81a8',1,'getAi_default.m']]],
  ['getaimultiexp_208',['getAimultiExp',['../d3/d5a/get_aimulti_exp_8m.html#a10582905917885bd4280c84f9afc825c',1,'getAimultiExp.m']]],
  ['getchainrulefactor_209',['getChainRuleFactor',['../d0/d59/simulate___llamosi2016_8m.html#ad2edaeff3e435cef9e65433e4656eb05',1,'getChainRuleFactor(in pscale, in parameterValue):&#160;simulate_Llamosi2016.m'],['../dc/de3/simulate___transfection2018__log__syms_8m.html#ad2edaeff3e435cef9e65433e4656eb05',1,'getChainRuleFactor(in pscale, in parameterValue):&#160;simulate_Transfection2018_log_syms.m']]],
  ['getchainrulefactors_210',['getChainRuleFactors',['../d0/d59/simulate___llamosi2016_8m.html#ae49d26a2d42a208c9de173495f183fb9',1,'getChainRuleFactors(in pscale, in theta, in sens_ind):&#160;simulate_Llamosi2016.m'],['../dc/de3/simulate___transfection2018__log__syms_8m.html#ae49d26a2d42a208c9de173495f183fb9',1,'getChainRuleFactors(in pscale, in theta, in sens_ind):&#160;simulate_Transfection2018_log_syms.m']]],
  ['getebayesests_211',['getEbayesEsts',['../dd/d16/get_ebayes_ests_8m.html#a4a0f751eaf4d294fc20368378932c8e8',1,'getEbayesEsts.m']]],
  ['getfsestimates_212',['getFSestimates',['../d7/dc5/get_f_sestimates_8m.html#a87ab857f9ec91580e07934f950f00e82',1,'getFSestimates.m']]],
  ['getfsuncertainities_213',['getFSuncertainities',['../dc/da6/get_f_suncertainities_8m.html#aef4a374a117da787e2209c569adda49b',1,'getFSuncertainities.m']]],
  ['getgtsoptions_214',['getGTSoptions',['../d3/d9c/get_g_t_soptions_8m.html#ae647b62533f002ee981fb9dc2fc63d7a',1,'getGTSoptions.m']]],
  ['gethessianapprox_215',['getHessianApprox',['../d5/dec/get_hessian_approx_8m.html#a91eb97cc5c2b99ceb12c24f9a9d86920',1,'getHessianApprox.m']]],
  ['getparametermatfn_216',['getParameterMatFn',['../d8/d21/get_parameter_mat_fn_8m.html#affa4728eebf2bb9736f6b1ed97eb9afa',1,'getParameterMatFn.m']]],
  ['getparametermatfn_5fdefault_217',['getParameterMatFn_default',['../d1/d7f/get_parameter_mat_fn__default_8m.html#a85ad14c5fadcd2072999430805a225d2',1,'getParameterMatFn_default.m']]],
  ['getparametervector_218',['getParameterVector',['../d0/deb/get_parameter_vector_8m.html#ab7a7536d02ddb5001ced6a4e34159fb8',1,'getParameterVector.m']]],
  ['getstableinverse_219',['getStableInverse',['../d7/d6f/get_stable_inverse_8m.html#ae25b5a84ac67e3f37713d82beb789f4e',1,'getStableInverse.m']]],
  ['getuncdirections_220',['getUncDirections',['../dd/d7e/get_unc_directions_8m.html#ae6e4f567c07bc6822b8aa694549715ca',1,'getUncDirections.m']]],
  ['getweights_5fdefault_221',['getWeights_default',['../d0/db3/get_weights_8m.html#ab476a4a5a65f378351984e84636f7729',1,'getWeights.m']]]
];
