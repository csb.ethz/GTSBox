var searchData=
[
  ['initializepopparams_60',['initializePopParams',['../d1/dbd/initialize_pop_params_8m.html#ab10ea8860d9c38fbaf84e63dd8eb0bb8',1,'initializePopParams.m']]],
  ['initializepopparams_2em_61',['initializePopParams.m',['../d1/dbd/initialize_pop_params_8m.html',1,'']]],
  ['initpaths_2em_62',['initPaths.m',['../d6/dab/_projects_2_llamosi2016_2init_paths_8m.html',1,'(Global Namespace)'],['../d6/df5/_projects_2templates_2init_paths_8m.html',1,'(Global Namespace)'],['../d7/d4c/_projects_2_transfection2018_2init_paths_8m.html',1,'(Global Namespace)'],['../dc/deb/_tests_2_indomethacin_2init_paths_8m.html',1,'(Global Namespace)']]],
  ['install_2emd_63',['INSTALL.md',['../d1/d58/_i_n_s_t_a_l_l_8md.html',1,'']]],
  ['instructions_20to_20install_20toolboxes_64',['Instructions to install toolboxes',['../d2/dc9/md___users_dlekshmi__desktop__repos__ph_d__g_t_s_box__i_n_s_t_a_l_l.html',1,'']]],
  ['instruction_20to_20make_20projects_65',['Instruction to make Projects',['../d3/db5/md___users_dlekshmi__desktop__repos__ph_d__g_t_s_box__projects__project_description.html',1,'']]],
  ['instructions_20for_20estimating_20nlme_20with_20gtsbox_66',['Instructions for estimating NLME with GTSBox',['../d2/d75/md___users_dlekshmi__desktop__repos__ph_d__g_t_s_box__src__n_l_m_e__r_e_a_d_m_e.html',1,'']]],
  ['indomethacin_67',['Indomethacin',['../de/dba/md___users_dlekshmi__desktop__repos__ph_d__g_t_s_box__tests__indomethacin__r_e_a_d_m_e.html',1,'']]]
];
