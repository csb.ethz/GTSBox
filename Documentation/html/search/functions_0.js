var searchData=
[
  ['callrungts_196',['CallRunGTS',['../d6/d19/_call_run_g_t_s_8m.html#a58ebca8235214b8d1161e6d415989219',1,'CallRunGTS.m']]],
  ['callsimulation_197',['callSimulation',['../d7/df5/_projects_2_llamosi2016_2_user_fns_2call_simulation_8m.html#a675cf3332f7c9140d55c01870b188396',1,'callSimulation(in varargin):&#160;callSimulation.m'],['../d2/d10/_projects_2_llamosi2016_2_user_fnsw_a_m_i_c_i_2call_simulation_8m.html#a675cf3332f7c9140d55c01870b188396',1,'callSimulation(in varargin):&#160;callSimulation.m'],['../d3/d84/_src_2_n_l_m_e_2_auxiliary_2call_simulation_8m.html#a99a7658730331dcf394913c42a179f53',1,'callSimulation(in time_exp, in kinetic_param, in ydata_response, in theta_hat, in int_opts, in type):&#160;callSimulation.m'],['../dc/d93/_tests_2_indomethacin_2_user_fns_2call_simulation_8m.html#a52899f888c8185c803f32bae1ead8a93',1,'callSimulation(in t, in kinetic_params, in ydata, in noise_params, in int_opts, in varType):&#160;callSimulation.m']]],
  ['checkdataformat_198',['checkdataFormat',['../dd/df7/checkdata_format_8m.html#a4a7a2190c82c1f9f662a8b6dbac43eae',1,'checkdataFormat.m']]],
  ['checkdatawlinformat_199',['checkdatawLinFormat',['../d1/d91/checkdataw_lin_format_8m.html#a30309b5d3ea3d932fad3144a5fe9cc27',1,'checkdatawLinFormat.m']]],
  ['class2struct_200',['class2struct',['../dd/d93/class2struct_8m.html#a4563c7c179e8bb67143fb5ad990abe1c',1,'class2struct.m']]],
  ['convertforelich2018data_201',['convertForelich2018Data',['../dc/d84/convert_forelich2018_data_8m.html#a63e212eac486a41778fc53ec5ac9ab06',1,'convertForelich2018Data.m']]]
];
