var searchData=
[
  ['sampleposterior_2em_183',['samplePosterior.m',['../df/d2e/sample_posterior_8m.html',1,'']]],
  ['samplesinglecellestimates_2em_184',['sampleSinglecellEstimates.m',['../df/df5/_llamosi2016_2_user_fns_2sample_singlecell_estimates_8m.html',1,'(Global Namespace)'],['../df/d85/_transfection2018_2_user_fns_2sample_singlecell_estimates_8m.html',1,'(Global Namespace)']]],
  ['samplewols_2em_185',['sampleWOLS.m',['../d9/d31/_llamosi2016_2_user_fns_2sample_w_o_l_s_8m.html',1,'(Global Namespace)'],['../d1/d20/_transfection2018_2_user_fns_2sample_w_o_l_s_8m.html',1,'(Global Namespace)']]],
  ['secondstage_2em_186',['SecondStage.m',['../d1/d10/_second_stage_8m.html',1,'']]],
  ['simulate_5fllamosi2016_2em_187',['simulate_Llamosi2016.m',['../d0/d59/simulate___llamosi2016_8m.html',1,'']]],
  ['simulate_5ftransfection2018_5flog_5fsyms_2em_188',['simulate_Transfection2018_log_syms.m',['../dc/de3/simulate___transfection2018__log__syms_8m.html',1,'']]],
  ['ssobjectivefn_2em_189',['SSObjectiveFn.m',['../da/db2/_s_s_objective_fn_8m.html',1,'']]],
  ['ssobjectivefn_5fdefault_2em_190',['SSObjectiveFn_default.m',['../d8/d1b/_s_s_objective_fn__default_8m.html',1,'']]]
];
