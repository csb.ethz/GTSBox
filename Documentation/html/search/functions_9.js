var searchData=
[
  ['sampleposterior_233',['samplePosterior',['../df/d2e/sample_posterior_8m.html#ab9f6822a2fb598a8f4eff067e0d026ac',1,'samplePosterior.m']]],
  ['samplesinglecellestimates_234',['sampleSinglecellEstimates',['../df/df5/_llamosi2016_2_user_fns_2sample_singlecell_estimates_8m.html#af1c077c6209251e34d80064d0eae2d80',1,'sampleSinglecellEstimates(in singlecell_estimates, in nsamples, in Model):&#160;sampleSinglecellEstimates.m'],['../df/d85/_transfection2018_2_user_fns_2sample_singlecell_estimates_8m.html#af1c077c6209251e34d80064d0eae2d80',1,'sampleSinglecellEstimates(in singlecell_estimates, in nsamples, in Model):&#160;sampleSinglecellEstimates.m']]],
  ['samplewols_235',['sampleWOLS',['../d9/d31/_llamosi2016_2_user_fns_2sample_w_o_l_s_8m.html#a3162c9ca8db18e728617e0f6a1e154ef',1,'sampleWOLS(in t, in kinetic_params, in measurement_noise, in pop_params, in d, in int_opts):&#160;sampleWOLS.m'],['../d1/d20/_transfection2018_2_user_fns_2sample_w_o_l_s_8m.html#a3162c9ca8db18e728617e0f6a1e154ef',1,'sampleWOLS(in t, in kinetic_params, in measurement_noise, in pop_params, in d, in int_opts):&#160;sampleWOLS.m']]],
  ['secondstage_236',['SecondStage',['../d1/d10/_second_stage_8m.html#ad9db3ec9969ad1cc80d0dee59810f184',1,'SecondStage.m']]],
  ['simulate_5fllamosi2016_237',['simulate_Llamosi2016',['../d0/d59/simulate___llamosi2016_8m.html#aa8cf0f00388f5225a610972a57c412f5',1,'simulate_Llamosi2016.m']]],
  ['simulate_5ftransfection2018_5flog_5fsyms_238',['simulate_Transfection2018_log_syms',['../dc/de3/simulate___transfection2018__log__syms_8m.html#a01b4d982b78ce463151ffcbf920aee1d',1,'simulate_Transfection2018_log_syms.m']]],
  ['ssobjectivefn_239',['SSObjectiveFn',['../da/db2/_s_s_objective_fn_8m.html#a5744ab977d588eacd0c8cf96c0be2a45',1,'SSObjectiveFn.m']]],
  ['ssobjectivefn_5fdefault_240',['SSObjectiveFn_default',['../d8/d1b/_s_s_objective_fn__default_8m.html#a9e04464f7958c129051203911d37f9e1',1,'SSObjectiveFn_default.m']]]
];
