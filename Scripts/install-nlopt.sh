#!/bin/bash
# install the nlopt package

cd ../Dependencies/nlopt/

rm -r build
mkdir build; 
cd build

KERNEL=$(uname -s)


if      [ $KERNEL = "Darwin" ]; then
        KERNEL=mac
        cmake -DCMAKE_INSTALL_PREFIX=./ -DMatlab_ROOT_DIR=/Applications/MATLAB_R2017b.app ..	
	make
	make install

elif        [ $KERNEL = "Linux" ]; then
        KERNEL=linux
	cmake -DCMAKE_INSTALL_PREFIX=./ -DMatlab_ROOT_DIR=/net/bs-gridsw/sw-repo/bsse/MATLAB_R2017b ..
	make
	make install
elif    [ $KERNEL = "FreeBSD" ]; then
        KERNEL=linux
        
else
        echo "Unsupported OS"
fi

