#!/bin/sh
module load repo/grid
module load repo/bsse
module load grid/soge-8.1.9
module load repo/stelling

module load cmake/3.13.2
module load gcc/6.5.0
module load matlab/R2017b

module list
