This folder contains some scripts to test general functionalities

1. TestIndomethacinModel: runs GTS on example model and data
2. ModelCompileCheck.sh: checks if we can compile the Transfection model
3. checkdata(wLin)Format.m: checks if the mandatory columns are present in the data for using GTS
4. OptionsCheck.m (to check the options given, if needed)
5. Indomethacin/ : a model that does not involve AMICI toolbox
