%
% @brief callSimulation simulates the model, user defined file
%

function [time_points,observed_y,unobserved_states,observed_sens_y,Hessian,FIM,measurement_sd,residuals]=callSimulation(t,kinetic_params,ydata,noise_params,int_opts,varType)
% Model simulation, and FIM evaluation
model = @(phi,t)(phi(:,1).*exp(-phi(:,2).*t)+phi(:,3).*exp(-phi(:,4).*t));
model_sens=  @(phi,t) ([ exp(-phi(:,2).*t), -phi(:,1).*t.*exp(-phi(:,2).*t), exp(-phi(:,4).*t), -phi(:,3).*t.*exp(-phi(:,4).*t)]);
observed_y=model(kinetic_params,t);
time_points=t;
unobserved_states=[];


if ~isempty(noise_params)
observed_sens_y=model_sens(kinetic_params,t);
measurement_sd=varFun(noise_params,observed_y,varType);
residuals=ydata.Y-observed_y;
FIM=observed_sens_y'*inv(diag(measurement_sd.^2))*observed_sens_y;
Hessian=FIM; % to do

else
observed_sens_y=[];
measurement_sd=[];
residuals=[];
FIM=[];
Hessian=[];
    
end