%> @brief Initializes the relevant paths

addpath(genpath(['..',filesep,'..',filesep,'Src',filesep]))
addpath(genpath(['..',filesep,'..',filesep,'Tests',filesep]))
addpath(genpath(['..',filesep,'..',filesep,'Dependencies',filesep,'AMICI']))
addpath(genpath(['..',filesep,'..',filesep,'Dependencies',filesep,'nlopt']))
addpath(genpath(['..',filesep,'..',filesep,'Dependencies',filesep,'mcmcstat']))
addpath(genpath(['..',filesep,'..',filesep,'Dependencies',filesep,'PESTO']))
addpath(genpath(['.',filesep]))
