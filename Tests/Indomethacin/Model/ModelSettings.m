%> @brief Model setting class, specific to a project
%> Note this is part of a template created in the GTSBOX toolbox, 'xx' need to be replaced.

classdef ModelSettings
   properties
   %> Name of the model, string
      name = 'Indomethacin'

   %> function handle to simulate model, function handle starting with "@.."
      fnHandle = @simulate_response

   %> The parameters values for which sensitivities are not calculated, refers to 'k' in the sym file of AMICI, vector
      constants = [];

  %> The parameter names for which sensitivities are not calculated, refers to 'k' in the sym file of AMICI, vector
      constants_name = {};

   %> Default values of parameters, vector
      paramNominal = [1,1,1,1]

   %> Names of parameters, cell array with strings
      paramNames = {'p0','p1','p2','p3'}

   %> Log transform parameter values, true or false, if this is true also the model sym file should have the corresponding option in AMICI specified
      tolog = false

   %> Name of observation, string
      obsNames={'Y'};

   %> number of  states which need initial conditions
      nstates=1;

   %> Error model, cell array with strings for each observable, only 'additive' or 'multiplicative' is supported
      errorModel={'additive'};

   %> Initial value for the error model, cell array with vectors, each corresponding to one observable
      errorModelinit={[0.1]};

   %> ODE model integration absolute tolerance, integer
      abstol=1e-6;

   %> ODE model integration relative tolerance, integer
      rtol=1e-6;

   %>  ODE integration initial time point, integer
      t0=[0];

   %> Bounds Parameter bounds for individual parameters, vector
      paramLowerbounds=[0,0,0,0];

   %> Bounds Parameter bounds for individual parameters, vector
      paramUpperbounds=[100,100,100,100];
   end
end
