%>%> @brief FitIndomethacin runs a quick estimation of GTS on a small
%>%> dataset. The data is loaded from matlab and consists of drug
%>%> concentration measured in 6 subjects.
%>%>
%>%> More detailed description.
%>%>
%>%>
%>%> @[] result the result is saved in the respective result folder in the
%>%>            project folder
%>%>

initPaths()

% Make the data 
load indomethacin

data=[time,concentration, subject,ones(size(subject))];
data=array2table(data,'VariableNames', {'Time','Y','TrackID','YID'});

% Write the data in the data folder
writetable(data,'./DataForEstimation/data.csv')


%% Do the estimation
GTSoptions=getGTSoptions();
GTSoptions.FSopt.parWorkers=8;
GTSoptions.compileflag=0;
GTSoptions.testflag=1;
GTSoptions.runSecondStage=1;
GTSoptions.SSopt.inpfiles={'data'};
GTSoptions.SSopt.Optimization='LGTS_EM';

% CallRunGTS(model_name,datafolder,dataname,GTSoptions)
CallRunGTS('Indomethacin',[],'data',GTSoptions)

%
