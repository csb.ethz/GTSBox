%> @brief A function that checks if all mandatory columns are specified
function []=checkdataFormat(data)
mandatoryColumns={'Y','YID','Time','TrackID'};

if ~all(ismember(mandatoryColumns,data.Properties.VariableNames))
error('Data format is incorrect: mandator columns are Y, YID, Time, TrackID')
end

disp('Data columns are:');
disp(data.Properties.VariableNames);
end
