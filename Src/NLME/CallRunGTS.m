%>%> @brief Call function to run GTS estimation
%>%>
%>%> More detailed description.
%>%>
%>%> @param model_name name of the model (project)
%>%> @param datafolder name of the data folder
%>%> @param dataname name of the data file (data file)
%>%> @param GTSoptions options
%>%>
%>%> @[] result the result is saved in the respective result folder in the
%>%>            project folder
%>%>

function []=  CallRunGTS(model_name,datafolder,dataname,GTSoptions)


    % Add paths
    addpath(genpath(['NLME',filesep]))
%    addpath(genpath(['..',filesep,'Projects',filesep,model_name,filesep]))
    addpath(genpath(['..',filesep,'Dependencies']))

    if ~isempty(datafolder)
        dataDir=['..',filesep,'Projects',filesep,model_name,filesep,datafolder];
        addpath(genpath(dataDir))
    end

    RunGTS(model_name,datafolder,dataname, GTSoptions)
end
