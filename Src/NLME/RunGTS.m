%> @brief RunGTS calls the functions to run the GTS model
%>
%> More detailed description.
%>
%> @param model_name Name of the model
%> @param data_folder Data folder to be used for estimation
%> @param dataset_name: Name of the dataset to be used for estimation
%> @param GTSoptions: Options for GTS
%>
%> @[] result the result is saved in the respective result folder in the
%>            project folder
%>
%>

function [] = RunGTS(model_name, data_folder, dataset_name, GTSoptions)



testCase=GTSoptions.testflag;


% Model variables
 mySettings = eval(['ModelSettings']); % load model settings
 if testCase~=1
     Model=struct();
     % specific for AMICI, will no longer be used
     % modelFname=[mySettings.name,'_log_syms']; 
     % Model= eval([modelFname]);
     Model.settings=mySettings;
 else
     Model=struct();
     Model.settings=mySettings;
 end

% Prepare model
 if GTSoptions.compileflag == 1 % convert model
     disp('Preparing Model')
     disp('Generating simulation files');
     if(isfile(['.',filesep,'Model',filesep,'makeAmiModel.m']))
     disp('Making AMICI model')
         makeAmiModel;
     elseif(isfile(['.',filesep,'Model',filesep,'makeIQMModel.m']))
         disp('Making IQM model')
         makeIQMModel
     elseif(isfile(['.',filesep,'Model',filesep,'makeMexModel.m']))
         disp('Making model with makeMexModel function')
         makeMexModel
     end
 end

 % Get options for firststage
FSopt=GTSoptions.FSopt;

% Get Experiment data
if ~isempty(dataset_name)
disp('Loading Experiment Data')
data = readtable(fullfile(['.',filesep,'DataForEstimation',filesep, ...
   data_folder,filesep,dataset_name,'.csv']),'TreatAsEmpty','NA');
time = unique(data.Time);

if FSopt.runtype==2
checkdatawLinFormat(data);
else
checkdataFormat(data);
end

end

% Where to save the results
Model.outdir=['.',filesep,'ResultsNLME',filesep];
mkdir(Model.outdir)
Model.date=date();
Model.name=[Model.settings.name];
Model.parWorkers=FSopt.parWorkers;
Model.parProfile=FSopt.parProfile;
Model.showStatus=FSopt.showStatus;
Model.nstates=Model.settings.nstates;

% Anything related to the model simulation
Model.simulation.fn=Model.settings.fnHandle;
Model.simulation.options=1; % To Do
Model.simulation.k = Model.settings.constants;
Model.simulation.abstol=Model.settings.abstol;
Model.simulation.rtol=Model.settings.rtol;

if ~(isempty( Model.settings.t0))
  % check if the time zero for every cell is initialised else
  % the package uses time zero.
Model.simulation.t0=Model.settings.t0;
else
Model.simulation.t0=[];
end

% Anything related to the cell specific params
Model.indParams.toestimate=1:length(Model.settings.paramNominal);
if Model.settings.tolog==true
Model.indParams.inits=log(Model.settings.paramNominal);
else
Model.indParams.inits=Model.settings.paramNominal;
end
Model.indParams.numPars=length(Model.settings.paramNominal);
Model.indParams.namePars=Model.settings.paramNames;

% Set upper and lower estimation globals
if(FSopt.bounded==1)
if isempty(Model.settings.paramLowerbounds)
error('Bounds not specified, but FSopt.bounded is set to 1.')
else
Model.indParams.lowerbounds=Model.settings.paramLowerbounds;
Model.indParams.upperbounds=Model.settings.paramUpperbounds;
end
else
Model.indParams.lowerbounds = [];
Model.indParams.upperbounds = [];
end


% Noise model
ErrorModel=Model.settings.errorModel;
Model.noiseParams.type=ErrorModel;%repmat({ErrorModel},1,length(Model.settings.output_names));
Model.noiseParams.inits=Model.settings.errorModelinit; % TODO change this in model.settings.
Model.noiseParams.optim=FSopt.GLS.errorEstimation;

% Optimization
Model.optim.tols=FSopt.optimTols;
Model.optim.gloalg.name=FSopt.gloalgName;
Model.optim.gloalg.starts=FSopt.gloalgStarts;
Model.optim.gloalg.maxeval=FSopt.gloalgMaxeval;
Model.optim.alg.name= FSopt.algName;


% Flag that indicates if GLS should be done, or we stop with OLS.
Model.poolFlg=FSopt.poolFlg;

% Iteration related to the GLS:
% To Do make these as standard options
Model.GLS=FSopt.GLS;


% The result file from first stage
FSoutfile= [Model.noiseParams.optim,'_pool_',num2str(Model.poolFlg),'_Result_', model_name, '_', dataset_name];

if FSopt.runtype==1
disp(' Calling First stage ')
FirstStage(Model, data, FSoutfile);
elseif FSopt.runtype==2
disp(' Calling First stage ')
FirstStagewLineage(Model, data, FSoutfile);
end



optsecondStage=GTSoptions.SSopt;
optsecondStage.pNames=Model.settings.paramNames;
if(GTSoptions.runSecondStage==1)
% TODO: define the design matrices
% Which p[arameters are supposed to be the cluster parameters
% Define the design matrix accordingly
% identfies which parameter should be modelled as a covariate
% if ~isempty(optsecondStage.covariateTerm)
% pos=strcmp(Model.settings.names,...
%                     optsecondStage.covariateTerm);
% optsecondStage.covariate_term=find(pos);
% end

infiles=optsecondStage.inpfiles;

% Call Second Stage
if optsecondStage.jointExps==1
for inp_ids=1:length(infiles)
    if inp_ids==1
    inpfile ={['.',filesep,'ResultsNLME',filesep,Model.noiseParams.optim,'_pool_',num2str(Model.poolFlg),'_Result_', model_name, '_',infiles{inp_ids},'.mat']};
    else
    inpfile ={cell2mat(inpfile),['.',filesep,'ResultsNLME',filesep,Model.noiseParams.optim,'_pool_',num2str(Model.poolFlg),'_Result_', model_name, '_',infiles{inp_ids},'.mat']};
    end
end
outpath=['.',filesep,'ResultsNLME',filesep,optsecondStage.fname_append,'GTS_Result_', model_name, '_', cell2mat(infiles)];
% the name of the second stage result
disp(['Calling Second-Stage for ', cell2mat(infiles)])
SecondStage(inpfile,outpath,optsecondStage);
else

for inp_ids=1:length(infiles)
infile =['.',filesep,'ResultsNLME',filesep,Model.noiseParams.optim,'_pool_',num2str(Model.poolFlg),'_Result_', model_name, '_',infiles{inp_ids},'.mat'];
outpath=['.',filesep,'ResultsNLME',filesep,optsecondStage.fname_append,'GTS_Result_', model_name, '_', infiles{inp_ids}];	   % the name of the second stage result

disp(['Calling Second stage for ', infile])
SecondStage(infile,outpath,optsecondStage);
end
end
end


end
