#### Instructions for estimating NLME with GTSBox
This documents explains the options that should be used to estimate an NLME model with the repo.

To estimate the NLME model with this repo, the user has to first do the following.
1. Create  a project folder and specify the model settings, and format the data that can be used (See [Instructions to set up a project](../Projects/ProjectDescription.md).
2. Use a function within the Project folder to call the `CallRunGTS.m` function. Usually, we call this as `Fit_.m` (See example project's [README](../Projects/Transfection2018/README.md)).
3. To the CallRunGTS function, the user must pass the location of the data and options.



The `src/NLME` subfolder contains the relevant scripts to GTS. The description of all the options that have to be passed are in the `getGTSoptions.m` help page, of which some are highlighted here. The options to be used are initialized by calling the function:

      GTSopts=getGTSoptions()

Once this is done, the options can be modified as one would modify a MATLAB struct. This structure has two substructures named `FSopt` and `SSopt` which contain options to the first stage and the second stage respectively.

                     GTSopts =

                     struct with fields:

                     FSopt: [1×1 struct]
                     SSopt: [1×1 struct]
                     testflag: 0
                     compileflag: 1
                     runSecondStage: 0

The two stages are called in the `CallRunGTS.m` function, which is in turn called by the user from their project folder (refer to the page on how to define your projects). CallRunGTS calls the `RunGTS.m` function, where the different options are used to initialize the estimation procedure, data is read in and the names of the result files are determined. In the end the `.mat` files are stored in the `ResultsNLME` folder. As the names of the folders suggest, all functions related to the first and second stage are stored in separate folders.  `Auxiliary` folder consists of some default functions that are used.

For the first stage:
1. `Src/callSimulation.m` that calls the model simulation in AMICI
2. `getWeights.m` gets initial weights using a spline approximation of the observables in a single cell for WOLS. By default, we assume that the weight is proportional to the function value.

Both these can be overridden, if the functions with the same name are found in `UserFns/`  folder.

For the second stage:
1. `getAi_default.m` gets the default design matrix in the second stage for fixed effects. This can be overridden by the user specifying the covariate function as a function handle to the option for `GTSoptions.SSopt.CovariateFn`.

2. `getParameterMatFn_default.m` A default function that maps parameter vector and matrices in the second stage.  This is not used if the user specifies their own objective functions using `GTSoptions.SSopt.optim.fnHandle` and function handler `GTSoptions.SSopt.optim.fnParamMapper` that maps a vector of parameter to appropriate mean and covariance parameters.

### FirstStage
In the folder `FirstStage`, we carry out the first stage optimization method using the GLS-AR-POOL method for the generalized least squares method. The GTS begins with an initial estimation of all parameter estimates for every cell. If only one readout is measured in a cell, then ordinary least squares or OLS is used. If multiple readouts are observed, WOLS is used. After obtaining the initial estimates, the GLS-AR-POOL version iteratively estimates the measurement model parameters (as required by the user, be it an *additive or multiplicative* model, as indicated in the model settings class in a project).  

Currently, only the *pooled version* is implemented. If un-pooled option is given by a user, the first estimates using OLS or WOLS and the corresponding single-cell models are returned along with the residuals.

Three types of first stage can be done. First is by treating all cells independent (`FSopt.runtype=1`), or accounting for the lineage (`FSopt.runtype=2`). If you do not want to run the first stage, `FSopt.runtype` is given the value 0.

The options that are specific to the first stage and their default values are:

                        GTSoptions.FSopt

                        ans =

                        struct with fields:

                          runtype: 1
                          bounded: 1
                       parWorkers: 50
                       showStatus: 1
                        optimTols: 1.0000e-06
                       gloalgName: 39
                          algName: 29
                         gloalgStarts: 1000
                         gloalgMaxeval: 1000
                          poolFlg: 1
                              GLS: [1×1 struct]

#### FirstStage Results
The first stage results are found in the `ResultsNLME` folder. The results file consist of:

|Name|Dimensions|Desc.|
|----|:-----:|:-----:|
|AR_scaling   |               1 X number of observables |    The scaling parameter ($\eta$ in the Davidian's notation)|       
|Model      |                 1X1         | Model setting structure|             
|convergene_values|           1X1          | A structure with information on convergence|            
|cpu_end  |                 number of cores X 1 | Time spent in every core|             
|cptime |                   1X1  |               Time spent in total|        
|ids     |                  number of cells x1  | Track IDs|     
|sigma2_sqrt |            1 X number of observations|measurement scaling parameter, square root of sigma^2|      
|singlecell_estimates|        1X number of cells|A  structure with information for every cell|              
|theta_hat |               1 X number of observations| A cell with parameters of the measurement model|               
|theta_hat_sigma|           1 X number of observations |Measurement model parameters adjusted with the scaling parameter      |

The most relevant results for us from these fields is the `singlecell_estimates` field which consists of estimates, simulated model, residuals and integration options for every cell.

|Name|Desc.|
|----|:-----:|
|t| A vector of time points used for simulation|
|dat| A data table, Y, YID|
|params| A vector estimated parameters in a cell|
|obs_y| A vector or matrix simulated observables over time, number of time points X number of observables|
|FIM| Fisher information matrix for the cell's estimate, a square matrix|
|Hessian| Hessian (using second order sensitivities if required) of the cell's estimates, a square matrix|
|int_opts| Options used for ODE integration, a structure that was passed to callSimulation.m|
|exflg| The exit flag of the last optimization done, a number, refer to Nlopt doc|
|fval| The function value for WOLS, in the last iteration|
|cell_ID| The track ID of the cell|
|PopulationID| Experiment/ population ID (a covariate specified in the data file)|
|residuals| the raw residuals, same dimension as the simulated data|
|measurement_sd| The measurement model for a cell, same dimension as the simulated data|
|-----|-----|


An example that uses these for analysis is shown in the `Transfection` model example as an analysis notebook.

### SecondStage
In the folder `SecondStage`, the `SecondStage.m` file calls the estimation procedure.   The results from the first stage of a project are used as input to the second stage. The input files that should be passed are specified as a cell array:

    SSopt.inpfiles={};

If population parameters to be estimated are same for all the files, all the result files are loaded and concatenated before running the estimation. This is indicated with

    SSopt.jointExps=1
    SSopt.covariate_flag=[];



Two kinds of optimization can be done:

1. Using an optimizer, with `SSopt.Optimization=`'Optimizer';      
2. Using Expectation Maximization, `SSopt.Optimization=`'EM';      

#### Second stage using an optimizer
To use an optimizer, the user has to pass options `SSopt`, the options for the second stage and their default values are

                GTSoptions.SSopt

                ans =

                  struct with fields:

                      Optimization: 'Optimizer'
                       CovariateFn: @getAi_default
                          inpfiles: {}
                    covariate_flag: []
                             DiagD: 'full'
                          ncluster: 1
                         jointExps: 1
                      initialbeta0: []
                         initialD0: []
                      fname_append: []
                                EM: [1×1 struct]
                             optim: [1×1 struct]
                          fnHandle: []
                        CorrStruct: []
                           objOpts: [1×1 struct]


1. Options for the optimization passed as a struct `SSopt.optim`. The default options are:

              SSopt.optim.ftol_rel = 1e-6;
              SSopt.optim.xtol_rel = 1e-6;
              SSopt.optim.algorithm = NLOPT_LN_SBPLX;
              SSopt.optim.lower_bounds=[];
              SSopt.optim.upper_bounds=[];
The default algorithm is `NLOPT_LN_SBPLX`. Use other optimizations routines with NlOpt.
If you want to use other optimization toolboxes, all lines that use `nlopt_optimize` should be changed.

    [pop_param,fval,exflg] = nlopt_optimize(optim, parameters0);



2. Indicate if the variances are estimated in log-space to impose positivity

        SSopt.optim.tologVar=true;    


3. `SSopt.DiagD` is either 'full' or 'diagonal' or 'structured' (to be implemented)

4. Specify initial parameters

        SSopt.initialb0=[],
        SSopt.initialD0=[].


Unless they are specified, by default the mean and covariance matrix (depending on if we assumed a diagonal or full matrix) from the first stage are used as initial values.

5. `SSopt.optim.fnHandle` is the function handle to the objective function to be used. Unless a project specific objective function is used, the default one
 `SSObjectiveFn_default` is used. Usually, the parameters are passed as vectors to such functions and a mapping from vectors to the right dimensions of mean and covariance matrix is done with function `getParameterMatFn_default`. If a user specifies the objective function, the mapping also needs to be specified with `SSopt.optim.fnParamMapper`.

6. `SSopt.CovariateFn` is a function handle that returns the design matrix for each cell. Usually it takes a cell id or an ID indicating the type of a cell. The ID can be passed with `SSopt.covariate_flag`. If `SSopt.covariate_flag` is empty, all cells are of the same kind and the design matrix is assumed to be a diagonal matrix. For example, if the first 10 cells belong to type 0 and the next 10 belong to type 1, it can be idicated as:

	SSopt.covariate_flag=[zeros(1,10),ones(1,10)];

This ID can then be passed as an input to the user-specified `SSopt.CovariateFn`.


#### Second stage using EM
Several EM based algorithms can be used:
1. `EM`: The EM described by Marie Davidian in Chapter 5 of her book. This is called with `SSopt.Optimization`='EM'.


This is one of the simplest implementations, which enables us to estimate the mean and covariance matrix. So far, we only allow covariates for the fixed effects parameters and NOT the random effects. We also allow estimating diagonal and full covariance matrix. For other situations, currently please use optimization based estimation.


Options to be passed by the user:
1. `SSopt.DiagD` is either 'full' or 'diagonal' or 'structured' (to be implemented)

2. Initial parameters

        SSopt.initialb0=[];
        SSopt.initialD0=[];

3. Converge criteria to be followed in the EM. The defaults are:

        SSopt.EM.conv_maxiter=10000;
        SSopt.EM.conv_tol=5e-4;

4. `SSopt.CovariateFn` is a function handle that returns the design matrix for each cell. Usually it takes a cell id or an ID indicating the type of a cell. This ID can be passed with `SSopt.covariate_flag`. If `SSopt.covariate_flag` is empty all cells are of the same kind, and the design matrix is simply a diagonal matrix.

#### Result from second stage
In the result file from the second stage, there are

| Parameter | Desc. |
|-----------|------|
|D0   |                   Initial Population covariance  |                          
|Dhat  |                  Estimated Population covariance   |              
|Result |                 Results, a structure   |                       
|SSoptions |              Options used, a structure   |                 
|b0    |                  Initial mean vector    |                        
|betahat|                 Estimated mean vector    |                         
|betahati |               Empirical bayes estimates, a matrix,  number of cells X number of single-cell parameters   |   
|choleskyinv_flags|       Inversion issues in cells, a vector with error flags greater than zero which indicate cholesky based inversion failed, 1X number of cells   |   
|params|                  First stage estimates, a matrix, number of cells X number of single-cell parameters    |                 



In this, the most relevant thing for us is the `Result` structure, which is different when we use EM based algorithm or optimization. When we use EM,

| Parameter| Desc.|
|----|-----|
|PopCovar| Estimate of population covariance, a square matrix |
| eBayesEsts| EBayes estimates, number of cell X number of single-cell parameters|
|    PopMean| Estimate of population mean, a vector|
|        allMeans|All the mean parameters at every iteration, number of parameters X number of iterations|
|         allVars|All the covariance parameters at every iteration, number of parameters X number of iterations|
|   cluster_prob|Cluster probabilities if mixture models were used|
|          niter|Number of iterations, a number|
|        rblitys|Responsibilities, if mixture models were used|
|     Likelihood|Likelihood of the model |
|      designMat|Design matrices of the cells, i.e. Ai in the model, an N-D array|
|          invCi|Inverse of the uncertainty of the single cell estimates, an N-D array|
| covariate_flag|Cell identifier 0 or 1 indicating the type of the cell, a vector|
|       cell_IDs|Track IDs of the cells, a vector|

If we use an optimizer, the `Results` structure has:

|Parameter| Desc. |
|--------|-------------------------------------------------------- |
|PopCovar| Estimate of population covariance, a square matrix |
| eBayesEsts| EBayes estimates, number of cell X number of single-cell parameters|
|    PopMean| Estimate of population mean, a vector|
| covariate_flag|Cell identifier 0 or 1 indicating the type of the cell, a vector|
|       cell_IDs|Track IDs of the cells, a vector|
