% ======================================================================
%> @brief PlotPopPredictions is a function simulates the observables in the population 
%>
%> @param GTSResult GTSResult i.e the Results structure
%> @param EstimatedMeasurementNoise i.e. theta_hat in result.mat from the first stage
%> @param ModelStruct Model structure in the result.mat from the first stage
%> @param texp  Time points to simulate
%> @param nsamp number of samples to be drawn

%> @retval  simulatedData Table of simulated responses

% ======================================================================

function [simulatedData]= PlotPopPredictions(GTSResult,EstimatedMeasurementNoise,ModelStruct,texp,nsamp)

intopts.model=ModelStruct.simulation.fn;
intopts.abstol=ModelStruct.simulation.abstol;
intopts.rtol=ModelStruct.simulation.rtol;
intopts.t0=ModelStruct.simulation.t0;
intopts.k=ModelStruct.simulation.k;


PopMean=GTSResult.PopMean;
PopCovar=GTSResult.PopCovar;

simParams=mvnrnd(PopMean,PopCovar,nsamp);
noiseType=ModelStruct.noiseParams.type;
simulatedData=[];

for i=1:nsamp
    [t,y]=callSimulation(texp,simParams(i,:),[],[],intopts,[]);
    [ntimes,nobs]=size(y);
    meas_sd=varFun(EstimatedMeasurementNoise,y,noiseType);
    y_noise=y+meas_sd.*normrnd(0,1,size(meas_sd));
    y_noise=reshape(y_noise,ntimes*nobs,1);

    yid=reshape(repmat(1:nobs,ntimes,1),ntimes*nobs,1);
    t=repmat(t,nobs*ntimes/length(t),1);
    data_sim=[t,y_noise,yid,repmat(i,length(y),1)];

    simulatedData=cat(1,simulatedData,data_sim);
end
simulatedData=array2table(simulatedData,'VariableNames',{'Time','Y','YID','ID'});
