% ======================================================================
%> @brief PlotCellPredictions is a function simulates the observables in a cell 
%> @param singlecell_estimates The sturcture with cell specific estimates
%from the first stage i.e. singlecell_estimates
%> @param cellid index of the struct
%> @param nsamp number of samples to be drawn

%> @retval  simulatedData Table of simulated responses
%> @retval  observedData Table of observed responses

% ======================================================================
function [simulatedData,observedData]= PlotCellPredictions(singlecell_estimates,cellid,nsamp,ModelStruct)
%addpath(genpath(['..',filesep,'..',filesep,'..',filesep,'Projects',filesep,ModelStruct.name]))
%addpath(genpath(['..',filesep,'..',filesep,'..',filesep,'Dependencies',filesep,'AMICI']))
%addpath(genpath(['..',filesep]))



intopts=singlecell_estimates(cellid).int_opts;
CellPMean=singlecell_estimates(cellid).params;
if ~isfield(singlecell_estimates,'Post_Var')
CellPUnc=getStableInverse(singlecell_estimates(cellid).Hessian);
else
CellPUnc=getStableInverse(singlecell_estimates(cellid).Post_Var);
end

simParams=mvnrnd(CellPMean,CellPUnc,nsamp);
simulatedData=[];
texp=singlecell_estimates(cellid).t;

for i=1:nsamp
    [t,y]=callSimulation(texp,simParams(i,:),[],[],intopts,[]);
    [ntimes,nobs]=size(y);
    y=reshape(y,ntimes*nobs,1);
    yid=reshape(repmat(1:nobs,ntimes,1),ntimes*nobs,1);
    t=repmat(t,nobs*ntimes/length(t),1);
    data_sim=[t,y,yid,repmat(i,length(y),1)];
    simulatedData=cat(1,simulatedData,data_sim);
end
simulatedData=array2table(simulatedData,'VariableNames',{'Time','Y','YID','Sim_ID'});
observedData=singlecell_estimates(cellid).dat;
observedData.Time=singlecell_estimates(i).t;
end
