% ======================================================================
%> @brief  gets eigen vectors  corresponding to the lowest eigen value of the Hessian 
%>
%> @param Hessian Hessian calculated for the single-cell parameter estimates, a matrix


%> @retval  ev_uncertain_direction  Eigen vectors  corresponding to the lowest eigen value of the Hessian 
%> @retval  eigvec All eigen vectors
%> @retval  eigval All eigen values

function [ev_uncertain_direction, eigvec,eigval]=getUncDirections(Hessian)
[eigvec,eigval]=eig(Hessian);
eigval=diag(eigval);
[~,id]=min(eigval);
ev_uncertain_direction=eigvec(:,id);
end