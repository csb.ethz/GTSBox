%> @brief getGTSoptions initialses the options structure to be run in the GTS
%>
%> This function is called with no options, and intialises a struture. The structure can take the following fields indicated as parameters in this document.
%> @param testflag Flag indicating if it is test case, default is no=0
%> @param compileflag Flag that indicates if the model should be compiled, default is yes=1
%> @param FSopt.runtype Type of the first stage optimization that should be run. default is 1=without setting initial conditions to that in the mother, 0= donot run, 2= with lineage info (to evaluate)
%> @param FSopt.runSecondStage Option to pass if the second stage also needs to be run 0=no, 1=yes
%> @param FSopt.bounded Type of optimization to be used in the first stage 1=bounded; 0=unbounded. Default is 1. If no bounds are specified
%> @param FSopt.parWorkers Number of cores to be used in first stage, default is 10
%> @param FSopt.parProfile Cluster profile that should be used, default is 'local'
%> @param FSopt.showstatus Show the status of the optimization in first stage, true or false
%> @param FSopt.optimTols Tolerances in the optimizer, default is 1e-6 for both function/ parameter relative tolerances
%> @param FSopt.gloalgName Global Optimization to be used in first stage, the name of the algorithm an option to the optimizer used like NLOPT. Default is NLOPT_G_MLSL_LDS
%> @param FSopt.algName Local optimization to be used in first stage, the name of the algorithm an option to the optimizer used like NLOPT, Default is NLOPT_LN_SBPLX
%> @param FSopt.gloalg Numer of multi-starts/ population of parameters in the multi-start algorithm i.e. gloalg, default is 1000
%> @param FSopt.gloalgMaxeval Maximum number of function evaluations to use in the global optimization, default is 1e5. It is recommended you specify this, else there may be sitiations where you may have to wait for A LONG TIME to get any result. Usually, this number is enough based on experience
%> @param FSopt.poolFlg A flag that indicates if GLS (or only OLS) should be done 1=yes; 0=no. Default is 1. If 0 is specified, then the first  estimates using OLS or WOLS are used, and the model residuals for each cell is returned. For the first estimation, by default when only one readout is present, OLS is used. Else, WOLS is used, with a first estimate of the weights from `getWeights.m`, a default function that uses splines and approximates the mean function. The inverse of the spline based %>  approximation are used as weights. When WOLS is used, we recommend to pool all residuals and iterate through the GLS. An unpooled version of GLS is currently NOT implemented. The user can use their own way to intialise the weights if the `getWeight` function is specified in the project folder in `UserFns`.
%> @param FSopt.GLS.max_iters maximum iterations to be done in GLS,default is 10
%> @param FSopt.GLS.min_itersminimum iterations to be done in GLS, default is 2
%> @param FSopt.GLS.max_score relative difference criteria in GLS, default 0.01
%> @param FSopt.GLS.errorEstimation, error model estimation 'AR': absol. residuals, 'PL': PL, 'unpooled': unpooled, only for additive noises, default is 'AR'
%> @param SSopt.Optimization Type of secoond stage optimization, default is 'Optimizer';
%> @param SSopt.CovariateFn The function that returns the  design matrix for the covariates for fixed effects (i.e. Ai matrix in Davidian's notation). This should be found in function found in the model folder else, one of the generic functions @getAi_default is used
%> @param SSopt.inpfiles Results from first stage that should be used in the second stage, cell array of strings. The strings should be the in the form of data names of the First Stage, and not the full name of the '.mat file'
%> @param SSopt.covariate_flag A vector of numbers indicating if the cells belong to a certain subpopulation
%> @param SSopt.DiagD The kind of stucuture in the covariance matrix D. 'diagonal', 'full' or 'structured' (to do). The default is 'full'.
%> @param SSopt.jointExps If all cells in the experimentes in inpfiles options should be treated as one population of cells, yes=1 and no=0. The default is 1;
%> @param SSopt.initialbeta0 Initial values of the mean parameter, default is []
%> @param SSopt.initialD0 Initial values of the covariance matrix, default is []
%> @param SSopt.fname_append A string that you want start the name of the result file with, default is []

%> @param SSopt.optim, Optimizer specific options
%> @param SSopt.optim.fnHandle Function handle to be used for optimization, default is [] in which case a generic function is used.
%> @param SSopt.evalUncertainty A boolean variable to set if uncertainity of the second stage estimates need to be evaluated or not
%> @param SSopt.EM, EM specific options
%> @param SSopt.EM.conv_tol, Maximum absolute tolerance reached between parameter values of successive iterations, 5e-4
%> @param SSopt.EM.conv_maxiter, number of iterations in EM, default 10000

%> @param SSopt.optim.fnHandle Function handle to be used for optimization, default is [] in which case a generic function is used.
%> @param SSopt.optim.fnParamMapper Function handle to be used for converting vector of parameters to appropriate mean and covariance matrices, default is [] in which case a generic function is used.
%> @param SSopt.optim.ftol_rel relative  tolerance (for function evaluated) for NLOPT, default 1e-6
%> @param SSopt.optim.xtol_rel relative  tolerance (for parameter values) for NLOPT, 1e-6;
%> @param SSopt.optim.algorithm algorithm in NLOPT, default is NLOPT_LN_SBPLX;
%> @param SSopt.optim.lower_bounds Lower bounds used, default is []
%> @param SSopt.optim.upper_bounds Upper bounds used, default is []
%> @param SSopt.objOpts Options in second stage that should be passed to the first stage
%> @param SSopt.objOpts.tologVar Should the variance parameters be positive, true

%>retval GTSoptions A structure which contains all the parameters as the field

function [GTSoptions]=getGTSoptions()
testflag=0;

compileflag=1;

runSecondStage=0;

FSopt.runtype=1;

FSopt.bounded=1;

FSopt.parWorkers=10;

FSopt.showStatus=true;

FSopt.optimTols=1e-6;

FSopt.gloalgName=NLOPT_G_MLSL_LDS;

FSopt.algName=  NLOPT_LN_SBPLX;

FSopt.gloalgStarts=100;

FSopt.gloalgMaxeval=1e5;

FSopt.parProfile='local';

FSopt.poolFlg=1;

FSopt.GLS.max_iters=10;

FSopt.GLS.min_iters=2;

FSopt.GLS.max_score=0.01;

FSopt.GLS.errorEstimation='AR';


SSopt.Optimization='Optimizer';

SSopt.CovariateFn=@getAi_default;

SSopt.inpfiles={};

SSopt.covariate_flag=[];

SSopt.DiagD='full';

SSopt.jointExps=1;

SSopt.initialbeta0=[];
SSopt.initialD0=[];

SSopt.fname_append=[];

%convergence criteria for EM based algorithms in Second stage
SSopt.EM.conv_maxiter=10000;
SSopt.EM.conv_tol=5e-4;

% Optimizer criteria for optimization based algorithms in Second stage
SSopt.optim.ftol_rel = 1e-6;
SSopt.optim.xtol_rel = 1e-6;
SSopt.optim.algorithm = NLOPT_LN_SBPLX;
SSopt.optim.lower_bounds=[];
SSopt.optim.upper_bounds=[];

% Objective function to be passed to the second stage optimizer
SSopt.optim.fnHandle=[];
SSopt.optim.fnParamMapper=[];

% Flag to evaluate uncertainity in the second stage
SSopt.evalUncertainty=false; % user must define a function if needed

% Things that should be passed to the estimation objective function
SSopt.objOpts.tologVar=true;    % Used only during optimization


% Aggregate everything together
GTSoptions=struct();
GTSoptions.FSopt=FSopt;
GTSoptions.SSopt=SSopt;
GTSoptions.testflag=0;
GTSoptions.compileflag=1;
GTSoptions.runSecondStage=0;
end
