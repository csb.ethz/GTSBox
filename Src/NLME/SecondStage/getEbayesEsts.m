%%%------------------------------------------------------------------------
%> @brief getEbayesEsts gets the EBEs after the second stage estimation
%> @param betahat Estimated mean, vector
%> @param Dhat Estimated covariance, matrix
%> @param params Single-cell parameter estimates, a matrix
%> @param Ai Design matrix for every cell
%> @param invCi  Inverse of the uncertainty estimates, a 3-D array
%> @param covariate_flag  A vector of covariate labels

function betahati=getEbayesEsts(betahat, Dhat, params,Ai,invCi, covariate_flags)
ncov=length(unique(covariate_flags));
dummyvar_flag=generateDummyVar(covariate_flags,ncov);
betahati=zeros(size(params));
for  i=1:size(params,1)
    for k=1:ncov
        C_inv=invCi(:,:,i);
        if size(Dhat,3)>1
            D_inv=inv(Dhat(:,:,k));
        else
            D_inv=inv(Dhat(:,:,1));
        end
        betahati(i,:)=betahati(i,:)+(dummyvar_flag(i,k).*getStableInverse(C_inv+D_inv)*(C_inv*params(i,:)'+D_inv*Ai(:,:,i)*betahat'))';
    end
end
end