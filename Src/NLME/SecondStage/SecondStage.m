%%%------------------------------------------------------------------------
%SecondStage(infiles,outFile,SSoptions_SSoptionsimisation,[],SSoptionssecondStage);
%> @brief SecondStage does the second stage estimation of the GTS
%> @param inpFiles Names of the input data sets to use i.e. results from first stage
%> @param outFiles Name of the output file
%> @param compileflag Flag that indicates if the model should be compiled
%> @param SSoptions Options for the second stage
%> @param varargin  additional arguments (if needed)

function [Result]= SecondStage(inpFiles,outFile,SSoptions,varargin)

addpath(genpath(['.',filesep,'Auxiliary']));
addpath(genpath(['.',filesep,'SecondStage_objs']));

filename=outFile;       % File name to save results

%% Take results from multiple files and estimates together if needed
% input: SSoptions, inpfiles,
% output: singlecell_estimates, singlecell_estimates
[covariate_flags, singlecell_estimates]=getFSestimates(SSoptions,inpFiles);
SSoptions.objOpts.covariate_flags=covariate_flags; % to do: make this user specific

%% Find which cells may have issues with inversion:
% input: singlecell_estimates
% output: invCi, Ci, cholekyinv_flags
[Ci,invCi,choleskyinv_flags]=getFSuncertainities(singlecell_estimates);



%% Initialize the second stage optimization
cell_IDs=cat(1,singlecell_estimates.cell_ID);
if isfield(singlecell_estimates,'Post_Mean')
    params=cat(1,singlecell_estimates.Post_Mean);
else
    params=cat(1,singlecell_estimates.params);
end
nparams=size(params,2);
ncells=size(params,1);


%% Initialise the mean and covariance estimates based on the type of the optimization
% that is done.
% input: SSoptions, params
% out: b0, D_gls,  parameters0
[ b0, D0,  parameters0]=initializePopParams(SSoptions,params);



%% Make the matrix Ai, that encodes mapping from the vector of mean parameters
% and the cells
Ai=zeros(nparams,...
    length(b0),...
    ncells);

for i=1:ncells
    Ai(:,:,i)=SSoptions.CovariateFn(length(b0),covariate_flags(i));
end


%% Carry out EM
if strcmp(SSoptions.Optimization,'LGTS_EM')
    % Turn off the option for logVar
    SSoptions.tologVar=0;
    
    % convergence criteria for EM based
    conv=struct;
    conv.maxiter=SSoptions.EM.conv_maxiter;
    conv.tol=SSoptions.EM.conv_tol;
    ll_gts=@(p) LGTS_EM(b0,D0,params,invCi,Ci,conv,SSoptions.DiagD,...
        Ai);
    Result=ll_gts();
    %
    %     % Result
    betahat=Result.PopMean;
    Dhat=Result.PopCovar;
    betahati=Result.eBayesEsts;
    Result.covariate_flag=covariate_flags;

    fval=[];
    exflg=[];

    pop_param=getParameterVector(betahat,Dhat);
    
elseif strcmp(SSoptions.Optimization,'Optimizer')
    % Check for optimization file  in the folder if not, use the generic
    % function
    if isempty(SSoptions.optim.fnHandle)
        disp('Using the generic optimization function for the second stage')
        ll_gts=@(p) SSObjectiveFn_default(p,invCi,Ci,Ai,params,SSoptions.objOpts);
    else
        ll_gts=@(p) SSoptions.optim.fnHandle(p,invCi,Ci,Ai,params,SSoptions.objOpts);
    end
    optim=SSoptions.optim;
    optim.min_objective = ll_gts;

    if isempty(optim.lower_bounds)
        optim=rmfield(optim,{'lower_bounds','upper_bounds'});
    end

    [pop_param,fval,exflg] = nlopt_optimize(optim, parameters0);

    % Result
    if isempty(SSoptions.optim.fnHandle)
        [betahat, Dhat]=getParameterMatFn_default(pop_param,length(b0),SSoptions.objOpts.tologVar);
    else
        if isempty(SSoptions.optim.fnParamMapper)
        error('Specify the fn handle: SSoptions.optim.fnParamMapper')
        else
        [betahat, Dhat]=SSoptions.optim.fnParamMapper(pop_param,varargin);
        end

    end


end

%% Calculate the EBEs.
% input: betahat, Dhat, params, covariate_flags
% betahati
betahati=getEbayesEsts(betahat, Dhat, params,Ai, invCi,covariate_flags);

Result.PopMean=betahat;
Result.PopCovar=Dhat;
Result.eBayesEsts=betahati;
Result.covariate_flag=covariate_flags;
Result.cell_IDs=cell_IDs;
Result.inv_Ci=invCi;
Result.Ci=Ci;
Result.Ai=Ai;
Result.FSestimates=params;

%% Get the asymptotic covariance matrix using sampling
if SSoptions.evalUncertainty==true
    disp('Uncertainity evaluation called for'); 
    nsamp=1e3;
    if any(strcmp(SSoptions.Optimization,{'Optimizer','LGTS_EM'}))
    [parametersamples]=sampleGTSPosterior(pop_param,nsamp,SSoptions,Result);
   
    else 
        disp('Uncertainity evaluation not not done, option not valid'); 
        parametersamples=[];
    end

 else
   disp('Uncertainity evaluation not called for'); 
   parametersamples=[];
 end




%% Save the Result
save(sprintf('%s-%s-%s.mat',filename,SSoptions.Optimization,SSoptions.DiagD),...
        'Result','choleskyinv_flags','b0','D0','betahat','Dhat','betahati','params','SSoptions','exflg','fval','parametersamples')

disp('Result saved')

end
