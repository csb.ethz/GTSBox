%> @brief SSObjectiveFn_default is the default objective function which is
%> used in the second stage during optimization. You can specify your own for
%> instance,  "SSObjectiveFn" and place it in the porject folder. This can be
%> passed to the optimizer through the option 'fnHandle' in the second stage.

%> @param parameters A vector of parameters
%> @param invCi  Inverse of the uncertainty of the the cell-specific parameter estimates
%> @param Ai     3D array of the design matrices for the fixed effects
%> @param singlecell_estimates Estimates for the single cells, a matrix



function [obj_fn]=SSObjectiveFn_default(parameters,invCi,Ci,Ai,singlecell_estimates,varargin)

ncells=length(singlecell_estimates);
nbeta=size(Ai,2);
SSopt=varargin{1};


% parameterMapping: MAKE as a function
% all the mean of all the cells
[betahat,D]=getParameterMatFn_default(parameters,nbeta,SSopt.tologVar);

% check for positive definitiveness and proceed
p1=cholcov(D);
if ~any(isempty(p1))

    % Covariate_flags

    t1=0;
    t2=0;
    for i=1:ncells
            % Note: The choice of modelling the covariance this way is because
            % often we may want to impose some constraints on the covariance /
            % mean matrix. We could consider using a matrix to left and right
            % multiply a D matrix as well. But currently this is not
            % implemented.
            t1=t1+logdet((Ci(:,:,i)+D));
            t2=t2+(singlecell_estimates(i,:)'- Ai(:,:,i)*betahat')'*...
                getStableInverse(Ci(:,:,i)+D)*...
                (singlecell_estimates(i,:)'-Ai(:,:,i)*betahat');
    end

    obj_fn=t1+t2;

    if ~isreal(obj_fn)
        obj_fn=10^10;
    end
else
  %  disp('Covariance matrix not positive definite, excluding the parameter point! Consider using better bounds.');
    obj_fn=10^10;
end

end
