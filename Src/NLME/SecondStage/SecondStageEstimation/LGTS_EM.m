%> @brief Second Stage EM algotithm based on 5.12, 5.13 and 5.14 in Davidians book, Chapter 5.
%> @param invCi Inverse of the uncertainty estimates of the the single-cell parameters, 3D array
%> @param betahat0 Initial estimate of the mean, vector
%> @param Dhat0    Initial estimate of the covariance, vector
%> @param beta_ihat Estimates of the single-cell parameters, a matrix
%> @param conv      Structure with convergence criteria
%> @param DiagD     Option indicating if the estimated covariance structure is diagonal 0 or 1
%> @param Ai    Design matrices for the fixed effects of each cell, 3D array

%> @retval Result  Result structure


function [Result]=LGTS_EM(betahat0,Dhat0,beta_ihat,invCi,C_i,conv,DiagD,Ai,varargin)
%% Initialize
betahat=betahat0;
D = Dhat0;
ncells=length(beta_ihat);

% convergence estimate
conv_flag=1;
maxiter=conv.maxiter;
tol=conv.tol;

% Covariate_flags
% ncov=length(unique(covariate_flag));
% covariate_flags=generateDummyVar(covariate_flag,ncov);

% Initialize
diffbeta=zeros(maxiter,1);
diffD=zeros(maxiter,1,1);
niter=0;                                              % iteration counter
allbetahats=zeros(length(betahat),maxiter,1);	  % stores the values of betahats at each iteration
allDs=zeros(size(D(:,:,1),1)^2,maxiter,1);	      % stores the values of D estimates at each iteration
diffs=[];


while conv_flag==1

    %% E-step: Get expected single-cell parameters given betahat and D
    beta_i=zeros(size(beta_ihat));
    D_inv=getStableInverse(D);
    for  i=1:ncells
        beta_i(i,:)=(getStableInverse(invCi(:,:,i)+D_inv)*(invCi(:,:,i)*beta_ihat(i,:)'+D_inv*Ai(:,:,i)*betahat'))'; %5.12
    end

    %% M step: Get the beta and D that maximize the likelihood

    betahat_c=betahat; %at iteration c
    D_c=D;             %at iteration c
    wi_t1=zeros(size(betahat,2),size(betahat,2),1); %term 1 of Weight below 5.13
    Wi=zeros(size(betahat,2),size(beta_i,2),ncells);

    %% estimate betahat

    for i=1:ncells
        wi_t1=wi_t1+Ai(:,:,i)'*D_inv*Ai(:,:,i); %sum over all D^-1, Ai(:,:,i)'*
    end

    for i=1:ncells
        Wi(:,:,i)=Wi(:,:,i)+getStableInverse(wi_t1)*Ai(:,:,i)'*D_inv;
    end

    betahat_tmp=zeros(ncells,size(betahat,2));
    for  i=1:ncells
        betahat_tmp(i,:)=(Wi(:,:,i)*beta_i(i,:)')'; % 5.13
    end

    betahat=sum(betahat_tmp);

    %% estimate D, given betahat


    D_t1=zeros(size(D));
    D_t2=D_t1;



    for i=1:ncells


        D_t1=D_t1+getStableInverse(invCi(:,:,i)+D_inv); %term 1 from 5.15
%         if(isnan(D_t1))
%             disp('Inversion issues detected; check model')
%         end
        betadiff=(Ai(:,:,i)*betahat'-beta_i(i,:)')';

        if DiagD==1 % If you use a diagonal matrix, then only report that
            D_t2=D_t2+diag(diag(betadiff'*betadiff));
        else
            D_t2=D_t2+(betadiff'*betadiff); %term 2 from 5.15
        end

    end

    D_t2=D_t2./size(beta_i,1);%sum(covariate_flags(:,k));
    D=D_t1./size(beta_i,1)+D_t2;



    diffD=abs(D_c-D);%./max(D0,D);
    diffs=cat(1,diffs,max(max(diffD)));

    diffbeta(niter+1)=max(abs(betahat_c-betahat));
    diffD(niter+1,1,1)=max(max(abs(D_c-D)));%./max(D0,D);

    allbetahats(:,niter+1,1)=betahat;
    allDs(:,niter+1,1)=reshape(D,size(D,2).^2,1);


    disp([max(diffbeta(niter+1)),max(max(diffD(:,:,1)))])

    niter=niter+1;
    conv_flag=~all([(max(diffbeta(niter+1)))<tol,max(max(diffD))<tol]);
end

diffbeta=diffbeta(1:niter);

% Report the likelihood here

t1=0;
t2=0;
for i=1:ncells
        t1=t1+log(det(C_i(:,:,i)+D));
        t2=t2+(beta_i(i,:)'-Ai(:,:,i)*betahat')'*getStableInverse(C_i(:,:,i)+D)*(beta_i(i,:)'-Ai(:,:,i)*betahat');
end

Likelihood=-t1-t2;

%% Make the reuslt structure
Result.PopCovar=D; %: Cov. matrix (TODO: make a meaningful object to store this)
Result.eBayesEsts=beta_i; %: ebayes individual estimates
Result.PopMean=betahat; %: mean parameter estimate
Result.allMeans=allbetahats(:,1:niter);          % convergence of parameter vector
Result.allVars=allDs(:,1:niter);                 %  convergence of D
Result.cluster_prob=[];               % cluster probablities
Result.niter=niter;                   % iteration number
Result.rblitys=[];                    % responsiblities
Result.Likelihood=Likelihood;
Result.designMat=Ai;
Result.invCi=invCi;


end
