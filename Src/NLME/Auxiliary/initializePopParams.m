%%%------------------------------------------------------------------------
%> @brief Initialises the mean and covariance parameters of the second stage optimization, either for EM or optimizer based.
%> @param SSoptions Options struct of the second stage. If the options
%> contains initializations by the user these are used, else the mean and
%> covariance from single cell parameter estimates are used.
%> @param singlecell_estimates Single-cell parameter estimates, a 2D array/ matrix
%> @retval beta0 Vector of initial mean estimates
%> @retval D0 Matrix or array of covariance matrices
%> @retval parameters0 Vector of initial mean estimates: first elements of
%> beta0, then varaiances followed by correlation terms. This is only
%> returned when the optimization is done through an optimizer when either
%> the user a user defines a matrix.
%and uses the default fnHandle for optimization.

function [ beta0, D0,  parameters0]=initializePopParams(SSoptions,singlecell_estimates)



% EM
if strcmp(SSoptions.Optimization,'LGTS_EM')


%     if ~isempty(SSoptions.covariate_term)
%         beta0=SSoptions.initialbeta0;
%         % TO DO: add error flag
%     else
        if(isempty(SSoptions.initialbeta0))
            disp('No initial values for mean estimate given: initialising with mean of single-cell estimates \n');
            beta0= mean(cat(1,singlecell_estimates));
        else
            beta0=SSoptions.initialbeta0;
        end
%     end

    if ~isempty(SSoptions.initialD0)
        D0=SSoptions.initialD0;
    else
        disp('No initial values for covariance estimate given: initialising with covariance of single-cell estimates \n');
        D0= cov(singlecell_estimates);
    end

    parameters0=[];


% Optimization
elseif strcmp(SSoptions.Optimization,'Optimizer')

        if(isempty(SSoptions.initialbeta0))
            disp('No initial values for mean estimate given: initialising with mean of single-cell estimates \n');
            beta0= mean(singlecell_estimates);
        else
            beta0=SSoptions.initialbeta0;
        end

    if ~isempty(SSoptions.initialD0)
        D0=SSoptions.initialD0;
      if ~isvector(D0)    % If we had inputed a matrix then vectorise it for the optimization
                                    % Else use it directly as it is
        if strcmp(SSoptions.DiagD, 'diagonal')
            disp('Estimating diagonal matrix D as requested \n');
            singlecell_estimates_d=diag(D0);   % vector
             if SSoptions.optim.tologVar==true
             singlecell_estimates_d=log(singlecell_estimates_d);
             end
        else
            var_d=diag(D0)';

            [~,CorrMat]=cov2corr(D0);
            CorrMat_tril=tril(CorrMat,-1);
            if all(CorrMat_tril==0)

             if SSoptions.objOpts.tologVar==true
             singlecell_estimates_d=log(var_d);
             else
             singlecell_estimates_d=[var_d];
             end

            else
             if SSoptions.objOpts.tologVar==true
               var_d=log(var_d);
             end
            singlecell_estimates_d=[var_d',CorrMat_tril(:)']; % vector
            end
        end
      else
          singlecell_estimates_d=D0;
      end
    else
        disp('No initial values for covariance estimate given: initialising with covariance of single-cell estimates \n');
        if strcmp(SSoptions.DiagD, 'diagonal')
            D0= diag(cov(singlecell_estimates));
            singlecell_estimates_d=diag(D0);         %vector


        elseif strcmp(SSoptions.DiagD, 'full')
            D0= cov(singlecell_estimates);
            [~,CorrMat]=cov2corr(D0);
            mapID=tril(ones(size(CorrMat)),-1);
            CorrMat_tril=CorrMat(mapID==1);
            var_d=diag(D0)';
             if SSoptions.objOpts.tologVar==true
               var_d=log(var_d);
             end
            singlecell_estimates_d=[var_d,CorrMat_tril(:)']; %vector

        elseif  strcmp(SSoptions.DiagD, 'structured')
            disp('todo: implement structured matrix as well');

        end
    end

    parameters0=[beta0,singlecell_estimates_d]; % All parameters aggregated

end
