
%> @brief getFSestimates  obtains the results of the single cell estimates from all experiments
%> @retval singlecell_estimates Struct of all the single cell estimates and information from each cell
%> @retval covariate_flags from both experiments
%> @param SSoptions Option struct for the second stage
%> @param inpFiles Cell array with names of the experiments 


function [covariate_flags, singlecell_estimates]=getFSestimates(SSoptions,inpFiles)




if SSoptions.jointExps==1
    
    allsingle_cell_estimates=[];
    nexps=length(inpFiles);
    
    for inp_ids=1:nexps
        fname=inpFiles{inp_ids};          
        load(sprintf('%s',fname))
        allsingle_cell_estimates=[allsingle_cell_estimates,singlecell_estimates];
        
    end
    singlecell_estimates=allsingle_cell_estimates;
    
    % an ID that indicates the experiment
    if isempty(SSoptions.covariate_flag)
    disp('Covariate flags for cells not give, assuming all cells in each data set have the same population')
    covariate_flags=ones(1,length(singlecell_estimates));
    else
    covariate_flags=SSoptions.covariate_flag; %[covariate_flags,repmat(inp_ids-1,1,length(singlecell_estimates))];
    end
else
    fname=inpFiles;          
    load(sprintf('%s',fname))
    if isempty(SSoptions.covariate_flag)
    disp('Covariate flags for cells not give, assuming all cells have the same population')
    covariate_flags=ones(1,length(singlecell_estimates));
    else
     covariate_flags=SSoptions.covariate_flag;    
    end

    
end
end