%%%------------------------------------------------------------------------
%> @brief Function etFSuncertainities  takes in the result of the first stage procedure and gets the uncertainty estimates and their inverse. 

%> @param singlecell_estimates  Result from first stage, struct
%> @retval Ci A 3-D array with uncertainty estimates from each cell
%> @retvalinvCi  A 3-D array with inverse of uncertainty estimates from each cell
%> @retval choleskyinv_flags  Flag for each cell indicating if cholesky inversion failed 0: no, 1: yes


function [Ci,invCi,choleskyinv_flags]=getFSuncertainities(singlecell_estimates)

% The uncertainty estimates (and the inverse) of the estimates
Ci=zeros(length(singlecell_estimates(1).params),...
    length(singlecell_estimates(1).params),...
    length(singlecell_estimates));
invCi=zeros(length(singlecell_estimates(1).params),...
    length(singlecell_estimates(1).params),...
    length(singlecell_estimates));

% a flag that indicates if inversion failed (cholesky)
choleskyinv_flags=zeros(1,length(singlecell_estimates));

for i=1:length(singlecell_estimates)
    % By default we use the second order sensitivity based Hessian and use
    % the first-order based FIM if the Hessian was non-invertible; If the
    % resut has sampling based approximation however, we use that. 
    if ~isfield(singlecell_estimates(i),'Post_Var')
        invCi(:,:,i)=singlecell_estimates(i).Hessian;
        if any(eig(invCi(:,:,i))<=0)
            invCi(:,:,i)= singlecell_estimates(i).FIM;
        end

        [ Ci(:,:,i),choleskyinv_flags(i) ] =getStableInverse(invCi(:,:,i));
        if any(isinf(Ci(:,:,i)))
            disp(['cell ',num2str(i),' has numerical error in its uncertainty approximation, setting its uncertainty estimate to a large value. Consider revisiting this']);
            Ci(:,:,i)=eye(size(Ci(:,:,i)))*10^15; 
        end
    else
        Ci(:,:,i)=singlecell_estimates(i).Post_Var;
        [invCi(:,:,i),choleskyinv_flags(i)]=getStableInverse(Ci(:,:,i));
    end
end
