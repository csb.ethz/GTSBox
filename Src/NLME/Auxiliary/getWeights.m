function weights=getWeights_default(data,varargin)
    % Define smoothing parameter
    time=unique(data.Time);
    nobs=max(data.YID);
    weights=zeros(length(time),nobs);

    % Construct splines (for all data) and find error values, after making sure
    for obs_idx=1:nobs

        % Get data
        ydata_response=data.Y(data.YID==obs_idx);

        % Fit splines
        avail = ~isnan(ydata_response);
        [f,g] = fit(time(avail),ydata_response(avail),'smoothingspline');
        splines_cell = feval(f,time);
        splines_cell(~avail)=NaN;
        weights(:,obs_idx)=splines_cell;
    end
end
