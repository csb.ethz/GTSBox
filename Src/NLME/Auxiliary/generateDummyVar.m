%> @brief generateDummyVar generates dummy variable coding
%> @param  covariate flag A vector of numbers indivating the covariate id
%> @param ncov The number of covariates
%> @retval covariate_coded A coded representation of the covariate flag, a matrix 

function covariate_coded=generateDummyVar(covariate_flag,ncov)

covariate_coded=zeros(size(covariate_flag,2),ncov);
    if ncov>1

covariate_coded(:,1)=covariate_flag';
covariate_coded(:,2)=1-covariate_flag';
    else
 covariate_coded=covariate_flag';
    end
end
