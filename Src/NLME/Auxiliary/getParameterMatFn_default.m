%%%------------------------------------------------------------------------
%> @brief A default function that takes in a vector of parameters and converts it to
%> the mean and covariance matrix used in second stage. A user may specify
%> their own and pass it as an option. 

%> @param parameters A vector of parameters
%> @param nbeta Number of parameters that correspond to the mean, a vector
%> @retval betahat  The mean of cell-specific parameters, a vector
%> @retval D The covariance of cell-specific parameters, a matrix or array

function [betahat, D]=getParameterMatFn_default(parameters,nbeta,varargin)
% To Do: avoid the symbolic calculation inside!
syms x; 
tologVar=varargin{1};
betahat=reshape(parameters(1:nbeta),1,nbeta);
dhatParams=length(parameters)-length(betahat);
if length(parameters)>nbeta*2
nvarparam=double(max(solve((x*x-x)/2+x==dhatParams)));
else
 nvarparam= dhatParams;
end
% all variance parameters
if     tologVar==true
Varparams=exp(parameters(nbeta+1:nbeta+nvarparam));
else
Varparams=parameters(nbeta+1:nbeta+nvarparam);
end

% If there are more, they are correlation parameters
if length(parameters)>nbeta*2
    CorrMat=zeros(nvarparam);
    Corr_tril=parameters(nbeta+nvarparam+1:end);
    mapID=tril(ones(nvarparam),-1);
    CorrMat(tril(mapID,-1)==1)= Corr_tril;

    % construct the correlation matrix
    CorrMat=CorrMat+CorrMat'+eye(nvarparam);
    D=corr2cov(sqrt(Varparams),CorrMat);
else
    D=diag(Varparams);
end
end
