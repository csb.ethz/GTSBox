% ======================================================================
%> @brief varFun is a function that returns the variance model
%>
%> @param f_t Simulated observation over time
%> @param var_type Type of the measurement error model
%> @param noise_params   Noise parameters, a cell whose entries are the number of readouts

%> @retval  measurement_sd Measuremnt standard deviation of the observations
%> If varagin is given then only that particular thing is returned. If not
%only that one thing is? 
% ======================================================================
function measurement_sd=varFun(noise_params,f_t,var_type,varargin)

measurement_sd=zeros(size(f_t));

% GO through everythin if the observation ID is NOT given 
if isempty(varargin)

    if length(var_type)~=size(f_t,2)
        error('Size of the variance type argument and the observed responses not consistent: check model result/ initialization of error model.')
    end
    
    for n_obs=1:length(var_type)
        if iscell(noise_params)
            noise_params_obs=noise_params{n_obs};
        else
            noise_params_obs=noise_params;
        end
        
        type_obs=var_type{n_obs};
        if strcmp(type_obs,'additive')==0
            theta_0=noise_params_obs(1);
            theta_1=(noise_params_obs(2));
            measurement_sd(:,n_obs)=repmat(theta_0,size(f_t(:,n_obs)))+...
                theta_1*f_t(:,n_obs);
            
        else
            measurement_sd(:,n_obs)=repmat(noise_params_obs(1),size(f_t(:,n_obs)))+...
                0.*f_t(:,n_obs);
        end
    end
else
    n_obs=varargin{1};
    type_obs=var_type{n_obs};
    if iscell(noise_params)
        noise_params_obs=noise_params{n_obs};
    else
        noise_params_obs=noise_params;
    end
    if strcmp(type_obs,'additive')==0
        theta_0=noise_params_obs(1);
        theta_1=(noise_params_obs(2));
        measurement_sd=repmat(theta_0,size(f_t(:,n_obs)))+...
            theta_1*f_t(:,n_obs);
        
    else
        measurement_sd=repmat(noise_params_obs(1),size(f_t(:,n_obs)))+...
            0.*f_t(:,n_obs);
    end
end


end
