function [parameters0]=getParameterVector(betahat,Dhat)
nbeta=size(Dhat,2);
mapID=tril(ones(nbeta),-1);
[~,CorrMat]=cov2corr(Dhat);
corrs=CorrMat(tril(mapID,-1)==1);
varparams=diag(Dhat);
parameters0=[betahat,varparams',corrs'];
end
