%> @brief getAi_default gets the default design matrices for each cell's
%> fixed effect
%> @param nbeta is the number of parametrs pertaining ti the fixed effect/
%> mean
%> @retval Ai A diagonal matrix with nbeta X nbeta dimensions
function Ai=getAi_default(nbeta,varargin)


Ai=eye(nbeta);

end