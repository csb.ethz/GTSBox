function strct = class2struct(cls)
% class2struct(cls) returns the input 'flattened' to a struct -
% whether it is a class, or has embedded classes in cells / fields.
% (the case of embedded classes is when struct(cls) isn't enough).
% The output struct contains all the data of the input without 
% any properties, and can be easily serialized/deserialized or
% used in MEX.
%
%   Copyright 2016-2070 Ofek Shilon
initialWarnState = warning('query' ,'MATLAB:structOnObject');
initialWarnState = initialWarnState.state;
warning('off', 'MATLAB:structOnObject')
if isobject(cls)
    strct = struct(cls);
else
    if ~isstruct(cls) && ~iscell(cls)
        strct = cls;
        warning(initialWarnState, 'MATLAB:structOnObject');
        return
    end
end
% recurse into cells
if iscell(cls)
    for i=1:numel(cls)
        strct{i} = class2struct(cls{i});
    end
    strct = reshape(strct,size(cls));
    warning(initialWarnState, 'MATLAB:structOnObject');
    return
end
% iterate over struct arrays
if numel(cls)>1
    for i=1:numel(cls)
        strct(i) = class2struct(cls(i));
    end
    strct = reshape(strct,size(cls));
    warning(initialWarnState, 'MATLAB:structOnObject');
    return
end
% recurse into structs
strct = struct(cls);
warning(initialWarnState, 'MATLAB:structOnObject');
names = fieldnames(cls);
for i=1:numel(names)
    strct.(names{i}) = class2struct(cls.(names{i}));
end