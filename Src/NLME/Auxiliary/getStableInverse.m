
%> @brief getStableInverse gets the inverse of a matrix. First Cholesky is tried. If that fails,
%> 	and the condition number is zero, then a small number is added to the diagonal of the
%>	matrix. Then, the 'inv', based on LU decomposition is used.
%>
%> @param inpMat Input matrix
%> @retval inv_mat Inverse of inpMat
%> @retval err Error flag, 1 if the cholesky inverse failed


function [invMat,err]=getStableInverse(inpMat)

[U,err]=chol(inpMat); 		% Invert using Cholesky Factorization first
if(err==0)
invMat=inv(U)*inv(U)';
else

%disp('WARNING:Cholesky inverse failed, issues in matrix inversion likely');

% If matrix is working precision
if rcond(inpMat)==0
        inpMat=inpMat+eye(size(inpMat,1))*10^-10;
end

% Invert using LU decomposition
invMat=inv(inpMat);

end
