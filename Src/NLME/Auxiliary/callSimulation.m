%> @brief callSimulation is a generic function that calls the model simulation
%>
%> @param time_exp Time points to integrate, a vector
%> @param kinetic_param Parameter values, a vector
%> @param ydata_response Data of the cell, a data table
%> @param  theta_hat Parameters of the error model, a cell
%> @param int_opts ODE integration options, strut
%> @param type  Error model type, a cell
%> @retval  time_points Time points, a vector
%> @retval  observed_y Simulated mean response of the observable, vector
%> @retval  unobserved_states Simulated states, vector
%> @retval  observed_sens_y Sensitivities of the observable, matrix
%> @retval   Hessian Hessian around the parameter estimates calculated with AMICI, matrix
%> @retval  FIM Fisher Information Matrix around the parameter estimates calculated with AMICI, matrix
%> @retval sigma_y Measurement standard deviation at every time

function [time_points,observed_y,unobserved_states,observed_sens_y,Hessian,FIM,measurement_sd,residuals] = callSimulation(time_exp,kinetic_param,ydata_response,theta_hat,int_opts,type)


fn_handle=int_opts.model;
tsp0=time_exp;
if(isfield(int_opts,'t0'))
    t0=int_opts.t0;
else
    t0=tsp0(1);
end

% If Hessian and FIM need to be returned
if ~isempty(ydata_response)
    options = amioption('sensi',2,'sensi_meth','forward',...
        'atol',int_opts.abstol,'rtol',int_opts.rtol,'ism',1,'tstart',t0);
    n_obs=(unique(ydata_response.YID));
    simdat.Y=[];
    for( i=1:length(n_obs))
        simdat.Y=cat(2,simdat.Y,ydata_response.Y(ydata_response.YID==n_obs(i))); % TODO: make a separate function
    end
    simdat.Sigma_Y=varFun(theta_hat,simdat.Y,type);

    % if second order sensitivities were not computed, catch the error and
    % use only first order.
    MExc=[];
    try
    sol0=fn_handle(tsp0,kinetic_param,int_opts.k,simdat,options);
    catch MExc
        disp(MExc);
       disp('Using only first-order sensitivities and getting FIM');
       options = amioption('sensi',1,'sensi_meth','forward',...
        'atol',int_opts.abstol,'rtol',int_opts.rtol,'ism',1,'tstart',t0);
    sol0=fn_handle(tsp0,kinetic_param,int_opts.k,simdat,options);
    end

    % aggregate results.
    time_points=cat(1,sol0.t);
    observed_y=cat(1,sol0.y);
    unobserved_states=cat(1,sol0.x);

    % sensitivities
    observed_sens_y=sol0.sy;
    Hessian=-sol0.s2llh;
    FIM=sol0.sllh*sol0.sllh';
    measurement_sd=simdat.Sigma_Y;
    residuals=simdat.Y-sol0.y;
else
    MExc=[];
    try
    options = amioption('sensi',1,'sensi_meth','forward',...
        'atol',int_opts.abstol,'rtol',int_opts.rtol,'ism',1,'tstart',t0);
    sol0=fn_handle(tsp0,kinetic_param,int_opts.k,[],options);
    catch MExc
        disp(MExc);
        sol0.t=[];
        sol0.y=[];
        sol0.sy=[];
        sol0.x=[];
    end
      
    % aggregate results.
    time_points=cat(1,sol0.t);
    observed_y=cat(1,sol0.y);
    unobserved_states=cat(1,sol0.x);
    observed_sens_y=sol0.sy;
    FIM=[];
    measurement_sd=[];
    Hessian=[];    residuals=[];

end
