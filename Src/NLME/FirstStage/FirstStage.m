function FirstStage(model_struct,data_table, fname)
%
%> @brief FirstStage implements the First stage of the GTS.
%>
%> More detailed description.
%>
%> @param model_struct Model structure withr elevant information
%> @param data_table Table of data set, they should have the columns Y,
%>                          YID, TrackID
%> @param fname name that is added to the result file
%>
%>
%> @[] result the result is saved in the respective result folder in the
%>            project folder
%>


%   Lines that MAY have to modified by the USER:
%         a) parallellization options

warning('off','all')
addpath(genpath('FirstStage_objs'))             % path to objective functions used

Model=model_struct;
data=data_table;

% Output directory
out_dir=Model.outdir;                         % path to store the results

% parpool starts with local profile, change 'local' to any other cluster profile
c = parcluster(Model.parProfile);
c.NumWorkers = Model.parWorkers;
%parpool(c, c.NumWorkers);


% Load the data: data should contain columns Y, ID and TIME at the most.
ids = unique(data.TrackID);

%%%%%%%%%% Tweaking parameters: %%%%%%%%%%

% Initialise
beta_init =Model.indParams.inits;
theta_init = Model.noiseParams.inits;

n_kinetic_params=length(beta_init);

% Parameters that can be tuned:
% tolerances for optimizer convergence
x_tol=Model.optim.tols;
f_tol=x_tol;
x_tol_gls=x_tol;
f_tol_gls=f_tol;

% tolerances to the ODE SOLVER
int_opts=struct();

if isempty(Model.simulation.options)
    int_opts.abstol=1e-8;
    int_opts.rtol=1e-6;
    int_opts.model=Model.simulation.fn;
    int_opts.k=[];
else
    int_opts.abstol=Model.simulation.abstol;
    int_opts.rtol=Model.simulation.rtol;
    int_opts.model=Model.simulation.fn;
    int_opts.k=Model.simulation.k;
end


% Convergences
max_iter_GLS=Model.GLS.max_iters;
min_iter_GLS=Model.GLS.min_iters;
max_score=Model.GLS.max_score;

% Objective function used to estimate the noise parameters
n_cells=length(ids);

% Pre initialze the array to store the results of the individual fits.
x_ols = zeros(n_cells, n_kinetic_params);  % Store individual parameters
fval_ols = zeros(1, n_cells);              % Store the function values
exflg=zeros(1,n_cells);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Finding initial estimates
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
if ~isfile(['.',filesep,'UserFns',filesep,'callSimulation.m'])
            disp('Using the default simulation file')
end

t1 = tic;
spmd, cpu0 = cputime(); end
    n_observables=length(Model.noiseParams.type);

parfor i = 1: n_cells
    int_opts=struct();

    if isempty(Model.simulation.options)
        int_opts.abstol=Model.simulation.abstol;
        int_opts.rtol=Model.simulation.rtol;
        int_opts.model=Model.simulation.fn;
        int_opts.k=[];
        int_opts.t0=[];
    else
        int_opts.abstol=Model.simulation.abstol;
        int_opts.rtol=Model.simulation.rtol;
        int_opts.model=Model.simulation.fn;
        int_opts.k=Model.simulation.k;
        int_opts.t0=Model.simulation.t0;
    end
    t = unique(data.Time(data.TrackID == ids(i)));

    switch n_observables
        case 1
            fun = @(para) OLS(t,para,data(data.TrackID == ids(i),:) ,int_opts);
        otherwise
            % Get initial weights using splines
            % h: weights we get from fitting a spline on the individual cells
            if ~isfile(['.',filesep,'UserFns',filesep,'getWeights.m'])
                if i==1
            disp('Using the default weight calculation for WOLS based initial estimates')
                end
            end
            data_cell=data(data.TrackID == ids(i),:);
            h=getWeights(data_cell);
            fun = @(para) WOLS(t, para,  h,data_cell ,int_opts);
    end
    % Optimization settings
    opt = struct();
    if(~isempty(Model.optim.gloalg.name))
        opt.algorithm =  Model.optim.gloalg.name;
        opt.set_population = Model.optim.gloalg.starts;
        opt.local_optimizer.algorithm = Model.optim.alg.name;
        opt.local_optimizer.ftol_rel = 1e-4;
        opt.local_optimizer.xtol_rel = 1e-4;
        opt.maxeval= Model.optim.gloalg.maxeval;
    else
        opt.algorithm =  Model.optim.alg.name;
        opt.ftol_rel = f_tol;
        opt.xtol_rel = x_tol;
    end

    opt.min_objective = fun;

    % May need constarints if ODE integration fails due to pathological parameters
    if ~isempty(Model.indParams.lowerbounds)
        opt.lower_bounds=Model.indParams.lowerbounds;
        opt.upper_bounds=Model.indParams.upperbounds;
    end

    [x_ols(i, :), fval_ols(i), exflg(i)] = nlopt_optimize(opt, beta_init);

    if Model.showStatus==true
        disp(['First estimates for cell: ',num2str(i)]);
    end
end
t2 = toc(t1);
    if Model.showStatus==true
sprintf(' First estimates done')
    end

% This pooled estimation is run only if the pool flag is 1. If not
% the result from the first estimation above is returned as is to the user.
% along with the simulated model.
if Model.poolFlg==1

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% GLS-POOL
% Initialise with OLS estimate: beta_init.
beta_init = x_ols;

% iteration counters
itern = 0;
iternact=0;


% estimates in the first iteration
betai_gls = beta_init;
theta_gls = theta_init;
theta_hat=theta_init;

% Convergence score.
score = 10^5; % Initialise the score to a large number
diffs = [];   % to store convergence

% To store parameters
diff_noise = [];
diff_betas=zeros(n_cells,Model.indParams.numPars,max_iter_GLS);
diff_fval = [];

% Start GLS-POOL estimation
t3 = tic;

while (itern == 0 || ((itern < max_iter_GLS-min_iter_GLS) && (score >= max_score)))  % Convergence criteria

    % Evaluate the model at previous estimates.
    for_pool=struct();

   parfor i=1:n_cells

        int_opts=struct();

        if isempty(Model.simulation.options)
            int_opts.abstol=1e-8;
            int_opts.rtol=1e-6;
            int_opts.model=Model.simulation.fn;
            int_opts.k=[];
        else
            int_opts.abstol=Model.simulation.abstol;
            int_opts.rtol=Model.simulation.rtol;
            int_opts.model=Model.simulation.fn;
            int_opts.k=Model.simulation.k;
            int_opts.t0=Model.simulation.t0;
        end
        t = unique(data.Time(data.TrackID == ids(i)));


        [~, f_t]=callSimulation(t,betai_gls(i,:),[],[],int_opts); % Integrating with first order sensitivities
        for_pool(i).f_t=f_t;
   end

    for n_obs=1:length(Model.noiseParams.type)
    % Maximize noise parameters with
    if  strcmp(Model.noiseParams.optim,'AR')
        fun = @(para) get_AR_gm(for_pool,  para, data, ids,Model.noiseParams.type,n_obs);
    else
        fun = @(para) get_PL_gm(for_pool,  para, data, ids,Model.noiseParams.type,n_obs);
    end

    opt_noise = struct();
    opt_noise.algorithm = Model.optim.alg.name;
    opt_noise.min_objective = fun;
    opt_noise.ftol_rel = f_tol_gls;
    opt_noise.xtol_rel = x_tol_gls;
    %opt_noise.maxeval = 1;
    [theta_hat0, ~,~] = nlopt_optimize(opt_noise, theta_gls{n_obs});
    theta_hat{n_obs}=theta_hat0;
    end
    % initalizeing the array to store result of new individual
    % estimates
    betai_hat = zeros(n_cells, n_kinetic_params);

    % Construct weights with theta_hat and maximize parameters
   parfor i = 1: n_cells
        int_opts=struct();

        if isempty(Model.simulation.options)
            int_opts.abstol=1e-8;
            int_opts.rtol=1e-6;
            int_opts.model=Model.simulation.fn;
            int_opts.k=[];
        else
            int_opts.abstol=Model.simulation.abstol;
            int_opts.rtol=Model.simulation.rtol;
            int_opts.model=Model.simulation.fn;
            int_opts.k=Model.simulation.k;
            int_opts.t0=Model.simulation.t0;
        end
        t = unique(data.Time(data.TrackID == ids(i)));


        % Get the new weights
        h=varFun(theta_hat,for_pool(i).f_t,Model.noiseParams.type);

        % Re-estimate individual parameters with new weights
        fun = @(para) WOLS(t, para,  h, data(data.TrackID == ids(i),:),int_opts);
        opt = struct();
        opt.algorithm = Model.optim.alg.name;
        %opt.maxeval= 1e4;
        opt.min_objective = fun;

        % Using low tolerances here can lead to bad estimates.
        opt.ftol_rel = f_tol_gls;
        opt.xtol_rel = x_tol_gls;

        % May need constarints if ODE integration fails due to pathological parameters
        if ~isempty(Model.indParams.lowerbounds)
            opt.lower_bounds=Model.indParams.lowerbounds;
            opt.upper_bounds=Model.indParams.upperbounds;
        end
        [betai_hat(i, :), fval(i), exflg(i)] = nlopt_optimize(opt, betai_gls(i, : ));

        if Model.showStatus==1
            disp(['WOLS: Cell: ',num2str(i)]);
        end
    end

    % Get the convergence measurement and store, based on relative convergence
    score1 = max(max(abs((betai_hat - betai_gls)./ betai_gls)));
    score2 = max(abs(cell2mat(theta_hat)-cell2mat(theta_gls))./cell2mat(theta_gls));
    score = min(score1, score2);
    diffs = cat(1, diffs, cat(2, score2, score1));

    diff_noise = cat(1, diff_noise, theta_hat);
    diff_fval = cat(2, diff_fval, fval);


    % Set for iterator
    iternact = iternact + 1;
    diff_betas(:,:,iternact)=betai_hat;
    itern = max(0, iternact - min_iter_GLS);
    betai_gls = betai_hat;
    theta_gls = theta_hat;

end
else
    theta_hat=theta_init;
    disp('Unpooled estimation is not currently implemented, returning OLS/WOLS estimates from the intial run')
    warning('It is recommended to iterate over the GLS atleast once, currently this is implemented only for pooled estimation')
    for n_obs=1:n_observables
    theta_hat{n_obs}=ones(size(theta_hat{n_obs}));
    end

    betai_hat=x_ols;
    fval=fval_ols;
    exflg=exflg;
    diff_noise=[];
    diff_fval=[];
    diffs=[];
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Calculate final residuals using estimated kinetic and noise parameters
% Calculate the sigma from the residuals
for_pool = struct();
for i = 1: n_cells
    p =  betai_hat(i,:);
    int_opts=struct();

    if isempty(Model.simulation.options)
        int_opts.abstol=1e-8;
        int_opts.rtol=1e-6;
        int_opts.model=Model.simulation.fn;
        int_opts.k=[];
    else

        int_opts.abstol=Model.simulation.abstol;
        int_opts.rtol=Model.simulation.rtol;
        int_opts.model=Model.simulation.fn;
        int_opts.k=Model.simulation.k;
        int_opts.t0=Model.simulation.t0;
    end
    t = unique(data.Time(data.TrackID == ids(i)));
    [~,for_pool(i).f_t]=callSimulation(t,p,[],[],int_opts);

    for n_obs=1:size(for_pool(i).f_t,2)
        [for_pool(i).res(:,n_obs), for_pool(i).h(:,n_obs), for_pool(i).gm(:,n_obs)] = WLS_gm(for_pool(i).f_t,theta_hat, data(data.TrackID == ids(i), {'Y','YID'} ),Model.noiseParams.type,n_obs,0);
    end
end

% Calculate the estimate of sigma2 and the scaling parameter (eta, in book) (for AR)
sigma = (cell2mat({for_pool.res}')./cell2mat({for_pool.h}')).^2;
sigma2 = nansum(sigma) ./ (sum(~isnan(sigma),1) - size(betai_hat, 2) * size(betai_hat, 1));
if any(sigma2<0)
    sigma2(sigma2<0)=nansum(sigma(sigma2<0)) ./ (sum(~isnan(sigma(sigma2<0)),1));
end

e_ij = abs((cell2mat({for_pool.res}')./(sqrt(sigma2).*cell2mat({for_pool.h}'))));
E_eij = mean(e_ij);
AR_scaling=mat2cell( E_eij .* sqrt(sigma2), 1 , ones(1,n_obs));
sigma2_sqrt=mat2cell(sqrt(sigma2), 1 , ones(1,n_obs));

% Usually we always assume in the end the error is gaussian, and take the
theta_hat_sigma=cellfun(@times, theta_hat,sigma2_sqrt,'uni',0);


% Make directory to save results, and save all results in a structure and some variables in the MAT file.
mkdir(sprintf('%s',out_dir))
singlecell_estimates=struct();


% Get the uncertainities of the estimate!
    disp('Starting to Calculating the FIM/ Hessian with pooled residuals, if the user function is provided');
for i=1:size(betai_hat,1)
    singlecell_estimates(i).t=unique(data.Time(data.TrackID==ids(i),:));   % Time
    singlecell_estimates(i).dat=data(data.TrackID==ids(i),{'YID','Y'});    % data
    singlecell_estimates(i).params=betai_hat(i,:);            % estimates of parameters
    px=betai_hat(i,:);

     if ~isfile(['.',filesep,'UserFns',filesep,'getHessianApprox.m'])
            [~,obs_y,~,~,Hessian,FIM,measurement_sd,residuals] = callSimulation(singlecell_estimates(i).t,px,singlecell_estimates(i).dat,theta_hat_sigma,int_opts,Model.noiseParams.type);
     else
            [~,obs_y,~,~,Hessian,FIM,measurement_sd,residuals] = getHessianApprox(singlecell_estimates(i).t,px,singlecell_estimates(i).dat,theta_hat_sigma,int_opts,Model.noiseParams.type);
     end

    singlecell_estimates(i).obs_y=obs_y;
    singlecell_estimates(i).FIM=FIM;                          % FIM
    singlecell_estimates(i).Hessian=Hessian;                  % Hessian
    singlecell_estimates(i).int_opts=int_opts;                % Integration options
    singlecell_estimates(i).exflg=exflg(i);                   % Exit flags of the individual optimization of cells
    singlecell_estimates(i).fval=fval(i);                     % Objective value for parameter estimation in the last iteration
    singlecell_estimates(i).cell_ID=ids(i);                   % Cell IDs
    singlecell_estimates(i).Track_IDs=unique(data.TrackID(data.TrackID==ids(i),:));                   % TrackIDs
    if any(strcmp('PopulationID',data.Properties.VariableNames))
    singlecell_estimates(i).PopulationID=unique(data.PopulationID(data.TrackID==ids(i),:));    % Population ID if available
    else
    singlecell_estimates(i).PopulationID=1;
    end
    singlecell_estimates(i).residuals=residuals;                  % Final resiuals for the cell
    singlecell_estimates(i).measurement_sd=measurement_sd;        % Final measurement model
end


spmd, cpu_end = cputime() - cpu0;, end;
spmd, cptime = gplus(cpu_end);, end;
cpu_end = cell2mat(cpu_end(: ));
cptime = unique(cell2mat(cptime(: )));

convergene_values=struct();
convergene_values.diff_noise= diff_noise; % noise values at every iteration
convergene_values.diffs=diffs;            % convergence criteria at every iteration

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Save
mkdir(out_dir);

% Save results
save([out_dir, fname, '.mat'],...
    'convergene_values', 'singlecell_estimates','cpu_end',...
    'theta_hat_sigma','theta_hat','cptime','Model','ids','AR_scaling',...
    'sigma2_sqrt')
% end parallelization.
delete(gcp('nocreate'))
