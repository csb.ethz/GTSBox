% ======================================================================
%> @brief WOLS standard WOLS, weighted LS method.
%>
%> More detailed description.
%>
%> @param t Time alues in experiment
%> @param kinetic_params  Kinetic parameters, $\beta_i$ in the model
%> @param measurement_noise Measurement error model/ as weights
%> @param d data for the cell
%> @param  int_opts Options passed to the ODE integrator, structure
%>
%> @retval obj_WOLS The objective function for weighted least squares
%> 
% ======================================================================

function obj_WOLS=WOLS(t,kinetic_params,measurement_noise,d,int_opts)
    h=measurement_noise;
% Only use the obj_WOLSult if ODE integration did not fail.
if (any(h(:)==Inf)||sum(log(h(:))==-Inf)||~any(isreal(log(h(:)))))
    obj_WOLS=Inf;
    obj_WOLS_obs=NaN;
else
    [~,f_t]=callSimulation(t,kinetic_params,[],[],int_opts);
    n_obs=unique(d.YID);
    obj_WOLS_obs=zeros(1,length(n_obs));
    
    if isempty(f_t)
        obj_WOLS=NaN;
    else
    for obs_id=1:length(n_obs)
        
        d_obs=d.Y(d.YID==n_obs(obs_id));
        nonans=~isnan(d_obs);               % Are there NaN values in data?
        obj_WOLS_obs(obs_id)=(d_obs(nonans)-f_t(nonans,obs_id))'...
            *(diag((1./h(nonans,(obs_id))).^2))...
            *(d_obs(nonans)-f_t(nonans,(obs_id)));
        
    end
    obj_WOLS=sum(obj_WOLS_obs);
    end
    if isnan(obj_WOLS)
        obj_WOLS=Inf;
    end
end
