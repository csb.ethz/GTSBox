% ======================================================================
%> @brief get_PL_gm gets the pooled PL. Implementation is based on
%> "Some Simple Methods for Estimating Intraindividual Variability in Nonlinear Mixed
%> Effects Models"
%> Author(s): Marie Davidian and David M. Giltinan
%> Source: Biometrics, Vol. 49, No. 1 (MPL., 1993), pp. 59-73
%> https://www.jstor.org/stable/2532602?seq=1#page_scan_tab_contents
%> Please see the  appendix of the paper
%>  OR section 2.4.2 of Nonlinear Models for Repeated Measurement Data, Chapman & Hall, London
%>   1996 Reprint edition .
%>
%> @param sim_struct structure containing the simulated data for all
%>                        cells at their estimated kinetic pPLameters from
%>                       last iteration
%> @param noise_param Noise pPLameters, theta_hat in the model
%> @param data Data for the cells
%> @param  indexes Cell IDs (so fPL only those with all data)
%>
%> @retval obj_PL Objective function (pooled PL, in log)
%>
%======================================================================
function obj_PL=get_PL_gm(sim_struct,noise_params,data,indexes,type,n_obs)

obj_PL_i=zeros(1,length(indexes)); % initialising the individual PLs

% Getting the ingredients needed, residuals, noise model, and geometric mean of log noise model
% for each cell
for_pool=struct();
for i = 1: length(indexes)
[for_pool(i).res,for_pool(i).h,for_pool(i).gm]=WLS_gm(sim_struct(i).f_t,noise_params,data(data.TrackID==indexes(i),{'Y','YID'}),type,n_obs);
end

% If the log geometric mean does not exist for any cell
if any(cell2mat({for_pool.gm})==Inf)
    obj_PL=Inf;
else
% Calculate the PL objective function for each cell
g_dot=log(geomean(cell2mat({for_pool.h}')));
for i = 1: length(indexes)
    obj_PL_i(i)=sum(sum((for_pool(i).res.*exp(g_dot)./for_pool(i).h).^2));
end

% Pooled PL over all cells
obj_PL=log(sum(obj_PL_i));
end
end
