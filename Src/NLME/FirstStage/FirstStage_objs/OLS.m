% ======================================================================
%> @brief OLS does standard OLS (Ordinary Least Squares, OLS) 

%> @param t Time values in experiment
%> @param kinetic_params  Kinetic parameters, $\beta_i$ in the model
%> @param noise_params Noise parameters, $\theta$ in the model
%> @param d data for the cell
%> @param  int_opts Options passed to the ODE integrator, structure

%> @retval obj_OLS The objective function ordinary least squares
%> @retval obj_OLS_obs The objective function ordinary least squares for
%> every observation i.e. time point
% ======================================================================


function [obj_OLS,obj_OLS_obs]=OLS(t,kinetic_params,d,int_opts)


obj_OLS=0;
% If any paramater is infinity, then return the objective value to be large.
if any(kinetic_params==Inf)
    obj_OLS=Inf;
    obj_OLS_obs=NaN;
else
    [~,f_t]=callSimulation(t,kinetic_params,[],[],int_opts);
   
    if ~isempty(f_t) 
    % OLS estimate, only use obj_OLS if there are no Infinite values in the ODE solution.
    n_obs=unique(d.YID);
    obj_OLS_obs=zeros(1,length(n_obs));
    for obs_id=1:length(n_obs)
        
        d_obs=d.Y(d.YID==n_obs(obs_id));
        nonans=~isnan(d_obs); % Are there NaN values in data?
        obj_OLS_obs(obs_id)=(d_obs(nonans)-f_t(nonans,obs_id))'*(d_obs(nonans)-f_t(nonans,obs_id));
        
    end
    obj_OLS=sum(obj_OLS_obs);
    end
    if isnan(obj_OLS)||isempty(f_t) 
        obj_OLS=Inf;
    end
        
end

