
% ======================================================================
%> @brief WLS_gm is used by get_AR_gm and get_PL_gm.m
%>   Generates the things needed to compute the AR/ PL objective functions
%>
%> More detailed description.
%>
%> @param f_t Response simulated over time for a cell
%> @param noise_params Noise parameters, $\theta$ in the model
%> @param d Data for the cell
%> @params n_obs Number of readouts
%>
%> @retval res The residuals
%> @retval h The measurement error model
%> @retval gm log geometric mean noise function of the cell
%> 
% ======================================================================



function [res,h,gm]=WLS_gm(f_t,noise_params,d,type,n_obs,varargin)

if ~isempty(varargin)
    KeepNaN=1;
else
    KeepNaN=0;
end
if any(f_t==Inf) %If integration had failed
res=Inf;
h=Inf;
gm=Inf;
else

% Run model
h=varFun(noise_params,f_t,type,n_obs);
obs_id=unique(d.YID);

d_obs=d.Y(d.YID==obs_id(n_obs));
nonans=~isnan(d_obs); % find NaN values

% Return values needed to evaluate the COST function.
if (any(h(:)==Inf)||sum(log(h(:))==-Inf)||~any(isreal(log(h(:)))))
res=Inf;gm=Inf;h=Inf;
else  
    if KeepNaN==0
    f_t=reshape(f_t(:,n_obs),length(d_obs),1);
    h=reshape(h,length(d_obs),1);
	res= d_obs(nonans)-f_t(nonans);
	h=h(nonans);
	gm=1/length(h)*sum(log(h));
    else
    f_t=reshape(f_t(:,n_obs),length(d_obs),1);
    h=reshape(h,length(d_obs),1);
	res= d_obs-f_t;
	gm=1/sum(~isnan(h))*nansum(log(h));
    end
        
end

end
