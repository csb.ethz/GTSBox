[![Gitter](https://badges.gitter.im/GTSBox/community.svg)](https://gitter.im/GTSBox/community?utm_source=badge&utm_medium=badge&utm_campaign=pr-badge)

This toolbox enables us to run the GTS on single-cell data and has been checked on Mac and Linux systems. This repo builds on the work:

1. Dharmarajan, Lekshmi, et al. "A simple and flexible computational framework for inferring sources of heterogeneity from single-cell dynamics." Cell systems 8.1 (2019): 15-26.

**Please read this document carefully before proceeding.**

### Requirements
This toolbox is based on MATLAB. The examples use ODE integrators from IQMtools (tested on Linux, windows MATLAB2019b) and AMICI (till MATLAB 2017b). MATLAB toolboxes used in this repo are:
1. distrib computing toolbox
2. financial toolbox (for cov2corr function)
3. statistics toolbox
4. symbolic toolbox

An appropriate mex compiler for the matlab version in use is also required.

### Common issues and solutions
1. Cannot run the examples:

	a. Make sure that you have the right version of MATLAB for the AMICI toolbox. We recommend starting with the Llamosi example instead which does not depend on AMICI and uses IQMtools instead. Also note you can use any ODE integrator by replacing the `callSimulation` file.

	b. Make sure that you have only the paths relevant to the right project.

	c. We have observed minor changes in the results based on the system used if the model is ill-conditioned.

	d. Make sure to RECOMPILE your model (delete and recompile the model source files) if you change the model txt or amici file.


2. If the second stage does not converge:

	a. Please check the approximation of the uncertainties in your model.

	b. It is likely you have less number of cells to estimate your model.

3. All other issues, please report.

### Contents
1. Projects folder is where you should add your projects and run the estimation. Already, two example projects are provided.

2. Documentation folder is where the documentation using doxygen will be built.

3. Scripts folder contains shell scripts that pertain to installation.

4. Tests folder contains a small test case, and some simple tests for ensuring the data is the right format.

5. Dependencies folder has all requirements as submodules in the toolbox.


### Setup and running
1. __Clone the repo.__

		git clone  https://gitlab.com/csb.ethz/GTSBox GTSBox
		cd GTSBox
		git submodule update --init

2. __Install dependencies.__ All submodules will be in the Dependencies folder.
Go into the Dependencies folder and install the required ones. AMICI does not require additional installation while NLOPT does.
See `INSTALL.md` for more information on the Dependencies and installing it in a server/ grid home.

3. __Run a project__ To run GTS on your  project, create a project in the `Projects` folder. You can use the `make-project.sh` to create a template of a project folder. More information on the `Project` folder can be found in [Instruction to make Projects](./Projects/ProjectDescription.md) in the `Projects` folder.

The best way to get acquainted with the functions in this toolbox is by running the example project: `Projects/Llamosi2016/` OR `Projects/Transfection2018` and the page on [Instructions for estimating NLME with GTSBox](./Src/NLME/README.md).


To run the Llamosi2016 model,

a. Install  IQMtools on your system

b. Follow the scripts M1 to M4.

c. You should get a figure that corresponds to the model prediction quantiles and the data quantiles reported in the publication. Here the example uses sampling to calculate uncertainties (rather than second order sensitivities).



#### Important markdown files
Refer to these markdown files to:
1. [Installing instructions](INSTALL.md)
2. [Instruction to use GTSBox](./Src/NLME/README.md)
3. [Instructions to set up a project](./Projects/ProjectDescription.md)


### Building the documentation
To build the documentation from the code, we use [doxygen](http://www.doxygen.nl/). In MacOS, you can easily download it using

	sudo port install doxygen


To build the document, use the shell script in the Documentation subfolder.


	cd Documentation

	sh make-documentation.sh


Relevant pages on installation and running already available examples can be found in the documentation files in:

	open Documentation/html/index.html




### Contact
Submit an issue or contact 
