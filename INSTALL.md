### Instructions to install toolboxes
This tool depends on specific dependencies, which are in the `Dependencies` folder. These dependencies are pulled as submodules. As of 2020, the following instructions will enable you to implement the toolboxes, with the version of the dependencies we used. If other versions of these tools need to be used, please refer to the dependencies' source for any information required.

Before starting, make sure that you have:
1. `cmake` (atleast 3.3.0). In the cluster, you may have a module which can be used by calling:

			module load cmake/3.x.x

2. `MATLAB`.  Since we rely on AMICI, only versions until MATLAB2017b can be used without any issues.
	Please see the issues reported in AMICI https://github.com/ICB-DCM/AMICI/issues/307.
	In the cluster, you could load the Matlab module:

			module load matlab/R2017b

3. The version of `c++ compiler `that can be used by MATLAB
	Please see: https://www.mathworks.com/help/matlab/matlab_external/choose-c-or-c-	compilers.html and https://www.mathworks.com/help/matlab/matlab_external/changing-default-compiler.html
	In the server/ grid, you can use

			module load gcc/4.9.4

4. To compile the documentation, we need to install Doxygen in the system. This can be done in macOS by using macports or homebrew:      

				 port install doxygen

Also, we need to have perl installed.
In the server, this can be done by again loading the right module (in process).

### Installation scripts
The following scripts will enable you to install the toolboxes from bash. These have to be executed from the folder: `Scripts`

1. **doxymatlab** toolbox: https://github.com/simgunz/doxymatlab
	the doxymatlab toolbox enables us to use doxygen with matlab scripts. The doxygen file we use in the Documentation folder and can be modified.
Check out the README in the toolbox to make any modifications that are specific to the OS used. The scripts you may have to run are `convertToUnix.sh` and modify the `.pl` scripts.  

2. **AMICI** toolbox: https://github.com/ICB-DCM/AMICI
	We use the Matlab version of the toolbox functions to integrate the ODE model and forward sensitivities.
	AMICI is used in Matlab by just adding the path of the toolbox.

3. **Nlopt** toolbox: https://github.com/stevengj/nlopt
	We use this toolbox for optimization. To install nlopt, run the script `install-nlopt`

		cd Scripts
		sh install-nlopt.sh

Note: If running on the server, load all relevant modules using:

		source load-modules.sh

before installing nlopt.

Depending on your system, always ensure the gcc version you use is compatible with the `cmake` version while installing nlopt.
