function [x_t,f_t, dydp, dxdp, H2, H1,sigma_y, residuals] = getHessianApprox(t,para,ydata,noise_params,int_opts,noise_params_type)
% time_exp,px,ydata_response,theta_hat,int_opts,type

ME=[]; % Store error
try
% Parameters passed 
para=exp(para);
k_p=para(1); %m production
g_m=para(2); %m degradation
g_p=para(3); %p degradation
theta_hat0=noise_params{1};


k_m=10;

% fixed constants
addon=[0 2 3 10 14];% defines the time where the integration input changes

% times when input is added
 t_in=[30  60.0000   90.0000  120.0000  150.0000  180.0000  210.0000  241.9064  290.4352  372.1597  455.3190 510.8225  546.0478];

if(isfield(int_opts,'t0'))
t0=int_opts.t0;
t_start_idx=min(find(t_in>=t0));
else
    t0=0;
    t_start_idx=1;
end

fn_handle=int_opts.model;

times=[];
tau=exp(3.4);


if(isfield(int_opts,'init_sx'))
init_sx=int_opts.init_sx; 
else
    init_sx=zeros(12,3);
end
% input time
inp=t_in(t_start_idx);
int_opts.k(4)=inp;



% intial values
xs=[];
ys=[];
us=[];

xsensitivities_1=[];
xsensitivities_2=[];
xsensitivities_3=[];

sensitivities_1=[];
sensitivities_2=[];
sensitivities_3=[];


options = amioption('sensi',2,'ism',1,'sensi_meth','forward',...
'atol',1e-8,'rtol',1e-4,'tstart',t0,'maxsteps',10000);


% Seond order sensitivities: intialise empty array
Hessian=[];
Hessian2=[];
for i=t_start_idx:length(t_in)
    inp=t_in(i);
    int_opts.k(4)=inp+tau;
for j=1:length(addon)
    while(t0<t_in(i)+addon(j)+tau)
    simulation_time=linspace(t0, t_in(i)+addon(j)+tau);
    simulated=fn_handle(simulation_time,[k_p, g_m, g_p],int_opts.k,[],options);
    t0=t_in(i)+addon(j)+tau;
    x0=simulated.x(end,:)';
    int_opts.k(1:3)=x0;
    times=cat(1,times,simulated.t(1:end-1));
    xs=cat(1,xs,simulated.x(1:end-1,:));
    ys=cat(1,ys,simulated.y(1:end-1,1));
    us=cat(1,us,simulated.y(1:end-1,2));
    
    % first order sens: for observation
    sensitivities_1=cat(1,sensitivities_1, simulated.sy(1:end-1,1,1).*k_p);
    sensitivities_2=cat(1,sensitivities_2,simulated.sy(1:end-1,1,2).*g_m);
    sensitivities_3=cat(1,sensitivities_3,simulated.sy(1:end-1,1,3).*g_p);

    % first order sens: for second state
    xsensitivities_1=cat(1,xsensitivities_1, simulated.sx(1:end-1,2,1).*k_p);
    xsensitivities_2=cat(1,xsensitivities_2,simulated.sx(1:end-1,2,2).*g_m);
    xsensitivities_3=cat(1,xsensitivities_3,simulated.sx(1:end-1,2,3).*g_p);
    
    % Second order    
    Hessian=cat(1,Hessian(:,:,:,:),simulated.s2y(1:end-1,1,:,:)) ;  
    Hessian2=cat(1,Hessian2(:,:,:,:),simulated.s2x(1:end-1,2,:,:)) ;  

    % initialize sensitivity also for next iteration
    secondOrder=squeeze(squeeze(simulated.s2x(end,:,:,:)));
    init_sx=[squeeze(simulated.sx(end,:,:));vec2mat(secondOrder(:),9)'];

    options = amioption('sensi',2,'sensi_meth','forward',...
    'atol',1e-8,'rtol',1e-4,'tstart',t0,'maxsteps',10000,'sx0',init_sx);
end
end
end
simulation_time=linspace( 546.0478+tau+14, 700);
simulated=fn_handle(simulation_time,([k_p, g_m, g_p]),int_opts.k,[],options);

    times=cat(1,times,simulated.t(1:end));
    xs=cat(1,xs,simulated.x(1:end,:));
    ys=cat(1,ys,simulated.y(1:end,1));
    us=cat(1,us,simulated.y(1:end,2));

    % Observation sensitivities
    sensitivities_1=cat(1,sensitivities_1,simulated.sy(1:end,1,1).*k_p);
    sensitivities_2=cat(1,sensitivities_2,simulated.sy(1:end,1,2).*g_m);
    sensitivities_3=cat(1,sensitivities_3,simulated.sy(1:end,1,3).*g_p);

    % first order sens: for second state
    xsensitivities_1=cat(1,xsensitivities_1, simulated.sx(1:end,2,1).*k_p);
    xsensitivities_2=cat(1,xsensitivities_2,simulated.sx(1:end,2,2).*g_m);
    xsensitivities_3=cat(1,xsensitivities_3,simulated.sx(1:end,2,3).*g_p);
    
    % Second order       
    Hessian=cat(1,Hessian,simulated.s2y(1:end,1,:,:))  ; 
    Hessian2=cat(1,Hessian2(:,:,:,:),simulated.s2x(1:end,2,:,:)) ;
    
    f_t=interp1(times,ys(:,1),t);
    x_t=interp1(times,xs(:,1:3),t);
    dydp=[sensitivities_1 sensitivities_2 sensitivities_3];
    dxdp=[xsensitivities_1 xsensitivities_2 xsensitivities_3];

    dydp=interp1(times,dydp,t);
    dxdp=interp1(times,dxdp,t);
    dy2dpdq=interp1(times,Hessian,t);
    dx2dpdq=interp1(times,Hessian2,t);
   us=interp1(times,us,t);
    H2=zeros(3,3);
    H1=zeros(3,3);

 for kk=1:1
     % noise model
     theta_hat0=noise_params{kk};
     sigma_y=theta_hat0(1)+theta_hat0(2).*f_t(:,kk);
     ydata_obs=ydata.Y(ydata.YID==kk);

     % Check NaNs in the data and exclude them.
     ids=1:length(ydata_obs);
     nonans=~isnan(ydata_obs);
     dydp_obs(:,:,kk)=dydp(ids(nonans)',[1,2,3],kk);
     dy2dpdq_obs(:,kk,:,:)=dy2dpdq(ids(nonans)',kk,:,:);
     sigma_y=sigma_y(ids(nonans),:);% make same dimensions.
     
     % Hessian using the first order sensitivities: 
     H1= H1+dydp_obs(:,:,kk)'*get_stableinverse(diag(sigma_y.^2))*dydp_obs(:,:,kk);
     
    % Hessian using the second order sensitivities: 
    residuals=ydata_obs(ids(nonans))-f_t(ids(nonans),kk);

    for n=1:3
        for m=1:3
            for t=1:length(sigma_y)
       H2(m,n)=H2(m,n)-residuals(t).*(1./sigma_y(t).^2).*dy2dpdq_obs(t,kk,m,n).*para(m).*para(n);
            end
         dy2dpdq_obs(:,kk,m,n)= dy2dpdq_obs(:,kk,m,n).*para(m).*para(n) ;
        end
    end
    H2=H2+dydp_obs(:,:,kk)'*get_stableinverse(diag(sigma_y.^2))*dydp_obs(:,:,kk);

  end
catch ME
disp(ME)
end


if isempty(ME)==false
    f_t=zeros(size(t))+Inf;
    f_t=f_t';
    dydp=[];
    dy2dpdq=[];
    us=[];
    H1=Inf;
    H2=Inf;
    dxdp=[];
end
end

