function [t,f_t,x_t,dydp,H1] = callSimulation(varargin)
% time_exp,px,ydata_response,theta_hat,int_opts,type

ME=[]; % Store error
try
 t=varargin{1};
 para=exp(varargin{2});
 int_opts=varargin{5};
 
% Parameters passed 
k_p=para(1); %m production
g_m=para(2); %m degradation
g_p=para(3); %p degradation

% fixed constants
addon=[0 2 3 10 14];% defines the time where the integration input changes

% times when input is added
 t_in=[30  60.0000   90.0000  120.0000  150.0000  180.0000  210.0000  241.9064  290.4352  372.1597  455.3190 510.8225  546.0478];

if(isfield(int_opts,'t0'))
t0=int_opts.t0;
t_start_idx=min(find(t_in>=t0));
else
    t0=0;
    t_start_idx=1;
end

fn_handle=int_opts.model;

times=[];

% intial values
xs=[];
ys=[];
us=[];

if(isfield(int_opts,'init_sx'))
init_sx=int_opts.init_sx; 
else
    init_sx=zeros(3,3);
end
% input time
inp=t_in(t_start_idx);
int_opts.k(4)=inp;
tau=exp(3.4);

options = amioption('sensi',1,...
'atol',int_opts.abstol,'rtol',int_opts.rtol,'tstart',t0,'maxsteps',10000,'sx0',init_sx);

% intialise sensitivities vector 
sensitivities_1=[0,0];
sensitivities_2=[0,0];
sensitivities_3=[0,0];

% Second order sensitivities: intialise empty array
% TODO: Check if we miss a data that start within the input pulse.  
for i=t_start_idx:length(t_in)
     
    inp=t_in(i);
    int_opts.k(4)=inp+tau;
for j=1:length(addon)
    while(t0<t_in(i)+addon(j)+tau)
    simulation_time=linspace(t0, t_in(i)+addon(j)+tau);
    simulated=fn_handle(simulation_time,[k_p, g_m, g_p],int_opts.k,[],options);
    t0=t_in(i)+addon(j)+tau;
    x0=simulated.x(end,:)';
    int_opts.k(1:3)=x0;
    times=cat(1,times,simulated.t(1:end-1));
    xs=cat(1,xs,simulated.x(1:end-1,:));
    ys=cat(1,ys,simulated.y(1:end-1,:));

    
    % first order sens: for observation
    sensitivities_1=cat(1,sensitivities_1, simulated.sy(2:end,:,1).*k_p);
    sensitivities_2=cat(1,sensitivities_2,simulated.sy(2:end,:,2).*g_m);
    sensitivities_3=cat(1,sensitivities_3,simulated.sy(2:end,:,3).*g_p);

    % initialize sensitivity also for next iteration
    init_sx=squeeze(simulated.sx(end,:,:));

    % new options set 
    options = amioption('sensi',1,...
    'atol',int_opts.abstol,'rtol',int_opts.rtol,'tstart',t0,'maxsteps',10000,'sx0',init_sx);
    end
end
end
simulation_time=linspace(546.0478+tau+14, 700);
simulated=fn_handle(simulation_time,([k_p, g_m, g_p]),int_opts.k,[],options);

    times=cat(1,times,simulated.t(1:end));
    xs=cat(1,xs,simulated.x(1:end,:));
    ys=cat(1,ys,simulated.y(1:end,:));


    % Observation sensitivities
    sensitivities_1=cat(1,sensitivities_1,simulated.sy(2:end,1:2,1).*k_p);
    sensitivities_2=cat(1,sensitivities_2,simulated.sy(2:end,1:2,2).*g_m);
    sensitivities_3=cat(1,sensitivities_3,simulated.sy(2:end,1:2,3).*g_p);

    % Interpolate states/ observation for times we are interested in 
    f_t=interp1(times,ys(:,1),t);
    x_t=interp1(times,xs,t);

    dydp(:,:,1)=[sensitivities_1(:,1) sensitivities_2(:,1) sensitivities_3(:,1)];
   
    dydp=interp1(times,dydp,t);

    dxdp=squeeze(simulated.sx(end,:,:));
    H1=[];
    % noise model
    if ~isempty(varargin{4})
     
     noise_params=varargin{4};
      ydata=varargin{3};
      
     for kk=1
    noise_params_obs=noise_params{kk};
    sigma_y=noise_params_obs(1)+f_t*noise_params_obs(2);
    ydata_obs_data=ydata.Y(ydata.YID==kk);
      
     %Check NaNs in the data and exclude them.
     ids=1:length(ydata_obs_data);
     nonans=~isnan(ydata_obs_data);
     dydp_obs=dydp(ids(nonans),[1,2,3],kk);  
     sigma_y=sigma_y(ids(nonans),:);


     % Hessian using the first order sensitivities: 
     H1= dydp_obs'*get_stableinverse(diag(sigma_y.^2))*dydp_obs;
     
     end
    end
 
        

catch ME
disp(ME)
end

% incase there was an error
if isempty(ME)==false
    f_t=zeros(size(t))+Inf;
    f_t=f_t';
    dydp=[];
    H1=Inf;
    x_t=[];

end
end
