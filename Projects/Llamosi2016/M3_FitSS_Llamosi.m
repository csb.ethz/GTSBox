%> @brief Fits the Llamosi dataset, second stage

%> @retval [] All results stored in ResultsNLME folder

function []=M3_FitSS_Llamosi()


addpath(genpath(['..',filesep,'..',filesep,'Src',filesep,'NLME']))
addpath(genpath(['..',filesep,'..',filesep,'Dependencies']))
addpath(['..',filesep,'..',filesep,'Tests'])

addpath(('Model'))
addpath(genpath('UserFns'))


% Get the GTS options: a default structure
GTSoptions=getGTSoptions();

GTSoptions.FSopt.runtype=0;
GTSoptions.compileflag=0;
GTSoptions.FSopt.bounded=[];

% Run first stage only
GTSoptions.runSecondStage=1;


% Data to be used:
fname=['Data_full_Llamosi2016'];
GTSoptions.SSopt.inpfiles={fname};
GTSoptions.SSopt.Optimization='LGTS_EM';




CallRunGTS('Llamosi', '', fname,GTSoptions)
end