%> @brief Plots predictions after the second stage

%> @retval [] A plot


addpath(genpath(['.',filesep,'UserFns']))
addpath(genpath(['.',filesep,'Model']))
addpath(genpath(['..',filesep,'..',filesep,'Src',filesep,'AnalysisScripts']))
addpath(genpath(['..',filesep,'..','Dependencies']))

load('./ResultsNLME/GTS_Result_Llamosi_Data_full_Llamosi2016-LGTS_EM-full.mat')
load('./ResultsNLME/AR_pool_1_Result_Llamosi_Data_full_Llamosi2016.mat')

% load the data
data=readtable('./DataForEstimation/Data_full_Llamosi2016.csv'); 
texp=unique(data.Time);
ydata=reshape(data.Y,100,325);

% simulate
ncells=10000;
SimulatedData=PlotPopPredictions(Result,theta_hat_sigma,Model,texp,ncells);
simdata=reshape(SimulatedData.Y,100,ncells);

% Summarise the data and prediction summaries
figure
plot(texp',nanmedian(ydata'),'-k')
hold on 
plot(texp',quantile(ydata',0.025),'--k')
plot(texp',quantile(ydata',0.975),'--k')

plot(texp',nanmedian(simdata'),'-r')
plot(texp',quantile(simdata',0.025),'-r')
plot(texp',quantile(simdata',0.975),'-r')
xlabel('Time (h)')
ylabel('Protein (AU)')

