%
%>%> @brief sampleWOLS is the objective function used to sample the posterior
% distribution of parameters
%>%> @params t time vector
%>%>  @params kinetic_params vector of parameters to be passed to the simulation
%>%>  @params measurement_noise estimated measurement standard deviation terms, a vector or matrix for the single cell pop_params
%>%>  @params d data for the cell, a table
%>%>  @params int_opts options used for simulation, a structure
%
%>%>  @retval value of the objective function, log likelihood


function   res=sampleWOLS(t,kinetic_params,measurement_noise,pop_params,d,int_opts)

h=measurement_noise;
% Only use the result if ODE integration did not fail.
if (any(h(:)==Inf)||sum(log(h(:))==-Inf)||~any(isreal(log(h(:)))))
    res=-Inf;

else
    [~,f_t]=callSimulation(t,kinetic_params,[],[],int_opts);
    if isempty(f_t)
        res=Inf;
    else
    n_obs=unique(d.YID);
    res_obs=zeros(1,length(n_obs));

    for obs_id=1:length(n_obs)

        d_obs=d.Y(d.YID==n_obs(obs_id));
        nonans=~isnan(d_obs);               % Are there NaN values in data?
        res_obs(obs_id)=-0.5.*(d_obs(nonans)-f_t(nonans,obs_id))'...
            *(diag((1./h(nonans,(obs_id))).^2))...
            *(d_obs(nonans)-f_t(nonans,(obs_id)));
    end
    p_y_betai=sum(res_obs);

    if isempty(pop_params)
        p_betai_popparams=0;
    else
        p_betai_popparams=-0.5.*(kinetic_params-pop_params.mean)*getStableInverse(pop_params.covar)*(kinetic_params-pop_params.mean)';
    end
    res=p_y_betai+p_betai_popparams;
    end
end
