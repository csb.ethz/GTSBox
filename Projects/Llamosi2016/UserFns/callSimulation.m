function [t,f_t,x_t,dydp,Hessian, FIM, measurement_sd, residuals] = callSimulation(varargin)
% inputs: time_exp,px,ydata_response,theta_hat,int_opts,type
% IQM version: we don't integrate with first order sensitivities, dxdp,
% dxdp, dydp, Hessian, FIM  cannot be calculated (and are outputted as empty vectors)
% measurement_sd, residuals returned only if varargin{3}(ydata_response),
% and varargin{4}, noise_params is passed.{6} noise type is needed if we
% use a function to parameterise the measurement variance/ std. dev. 

ME=[]; % Store error
try
    t=varargin{1};
    para=exp(varargin{2});
    int_opts=varargin{5};
    
    % Parameters passed
    k_p=para(1); %m production
    g_m=para(2); %m degradation
    g_p=para(3); %p degradation
    
    % Determine simulation time (IQM can only take 1 timespan, we cannot give
    % t0 seperate in the options)
    if(isfield(int_opts,'t0'))
        t0=int_opts.t0;
    else
        t0=t(1);
        % t_run = [t];
    end
    
    if isempty(t0)
        t0=0;
    end
    
    % times when input is added
    t_in=[30  60.0000   90.0000  120.0000  150.0000  180.0000  210.0000  241.9064  290.4352  372.1597  455.3190 510.8225  546.0478];

    t_start_idx=min(find(t_in>=t0));
    
    %  Defines the time where the integration input changes
    addon=[0 2 3 10 14];% defines the time where the integration input changes
    tau=exp(3.4); % any delay that may arise in the experiment
    
    % Model
    fn_handle=int_opts.model;
    
    % Options
    options.abstol = int_opts.abstol;
    options.rtol = int_opts.rtol;
    
    % Result vector
    xs=[];
    ys=[];

    times=[];
    
    for i=t_start_idx:length(t_in)
       inp=t_in(i);
       int_opts.k(4)=inp+tau;
        
        for j=1:length(addon)
            while(t0<t_in(i)+addon(j)+tau)
                simulation_time=linspace(t0, t_in(i)+addon(j)+tau);
                
                %output=fn_handle(simulation_time, IC, param values,options); % It needs all parameters: estimated + constant
                output = fn_handle(simulation_time, int_opts.k(1:3),([k_p, g_m, g_p, int_opts.k(4), int_opts.k(5)]), options);
                
                % concat. results
                times=cat(1,times,output.time(1:end-1));
                xs=cat(1,xs,output.statevalues(1:end-1,:));
                ys=cat(1,ys,output.statevalues(1:end-1,ismember(output.states, 'p')));
                
                % set the integration for next round
                t0=t_in(i)+addon(j)+tau;
                x0=output.statevalues(end,:);
                int_opts.k(1:3)=x0;
            end
        end
    end
    
    % last round
    simulation_time=linspace(638+tau, 700);
    output = fn_handle(simulation_time, int_opts.k(1:3),([k_p, g_m, g_p, int_opts.k(4), int_opts.k(5)]), options);
    times=cat(1,times,output.time(1:end-1));
    xs=cat(1,xs,output.statevalues(1:end-1,:));
    ys=cat(1,ys,output.statevalues(1:end-1,ismember(output.states, 'p')));
    
    %format times
    times = reshape(times.',1,[]).';
    
    % interpolate for the data values
    y_t=interp1(times,ys(:,1),t);
    f_t=exp(-g_p*tau).*y_t;
    f_t = reshape(f_t, [], 1); %make sure it's outputted as 1 column)
    x_t=interp1(times,xs,t);
    
    
    dxdp = []; % sensitivities
    dydp = []; % sensitivities
    Hessian = []; % Hessian
    FIM = [];
    
    if(~isempty(varargin{3}))
        noise_params=varargin{4};
        ydata=varargin{3};
        var_type=varargin{6};
        
        ydata_obs=ydata.Y(ydata.YID==1);
        
        % Check NaNs in the data and exclude them.
        ids=1:length(ydata_obs);
        nonans=~isnan(ydata_obs);
        
        
        residuals=ydata_obs(ids(nonans))-f_t(ids(nonans),1);
        measurement_sd=varFun(noise_params,f_t,var_type);
    else
        residuals=[];
        measurement_sd=[];
    end
    
catch ME
    disp(ME)
end

% in case there was an error
if isempty(ME)==false
    f_t=zeros(size(t))+Inf;
    f_t=f_t';
    Hessian=Inf;
    x_t=[];
    dxdp = [];
    dydp = [];
    FIM = [];
    residuals=[];
    measurement_sd=[];
end
end
