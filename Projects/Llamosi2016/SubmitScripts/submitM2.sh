#!/bin/bash

#$ -V
#$ -q sc03.q
#$ -cwd
#$ -N sampleEstimates
#$ -pe make 40


## 
matlab  -nosplash  -r "M2_samplePosterior(); exit"
