%> @brief Fits the gene expression example
%> @params data_id Data trial ID
%> @params FigCase Figure setting
%> @params ParamCase Parameter setting

%> @retval [] All results stored in ResultsNLME folder

function []=M1_FitFS_Llamosi()


initPaths


% Get the GTS options: a default structure
GTSoptions=getGTSoptions();
GTSoptions.FSopt.gloalgName=[];
GTSoptions.FSopt.algName=NLOPT_LN_NELDERMEAD;
GTSoptions.FSopt.runtype=1;
GTSoptions.FSopt.parWorkers=8;
GTSoptions.compileflag=1;
GTSoptions.FSopt.showStatus=1;
GTSoptions.FSopt.optimTols=1e-6;
GTSoptions.FSopt.bounded=[];
GTSoptions.GLS.max_iters=2;

% Run first stage only
GTSoptions.runSecondStage=0;
GTSoptions.SSopt.inpfiles={fname};

% Data to be used:
fname=['Data_full_Llamosi2016'];


CallRunGTS('Llamosi', '', fname,GTSoptions)
end
