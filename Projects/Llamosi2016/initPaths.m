%> @brief initializes paths

addpath(genpath(['..',filesep,'..',filesep,'Src',filesep,'NLME']))
addpath(genpath(['..',filesep,'..',filesep,'Dependencies']))
addpath(['..',filesep,'..',filesep,'Tests'])

addpath(('Model'))
addpath(genpath('UserFns'))
