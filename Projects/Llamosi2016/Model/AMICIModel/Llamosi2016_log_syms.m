function [model] = Llamosi2016_log_syms()

% 
%model.param = 'log';


% create state syms
syms m pr inp



%% PARAMETERS ( for these sensitivities will be computed )

% create parameter syms
syms k_m g_m g_p k_p tau u_c t_in  m0 p0 inp0

x=[m pr inp];

% CONSTANTS
k=[m0, p0, inp0, t_in, k_m,tau];

% create parameter vector 
p = [k_p,g_m, g_p];

% create symbolic variable for t
syms t


%% SYSTEM EQUATIONS
xdot = sym(zeros(size(x)));

%% INITIAL CONDITIONS


u_c=((0.5*tanh(100.0*t_in - 100.0*t + 300.0) + 0.5)*(t_in - 1.0*t + 2.0) + (0.5*tanh(100.0*t_in - 100.0*t + 300.0) - 0.5)*(0.5*tanh(100.0*t_in - 100.0*t + 1000.0) - 1.0*(0.5*tanh(100.0*t_in - 100.0*t + 1400.0) + 0.5)*(0.5*tanh(100.0*t_in - 100.0*t + 1000.0) - 0.5)*(0.25*t_in - 0.25*t + 3.5) + 0.5))*(0.5*tanh(100.0*t_in - 100.0*t + 200.0) - 0.5);

%% SYSTEM EQUATIONS
xdot(1)=k_m*inp - g_m*m ;   %mRNA
xdot(2)=k_p*m - g_p*pr;    %Protein
xdot(3)=0.3968*u_c - 0.9225*inp;  %Actual input


%% OBSERVABLES

y = sym(zeros(2,1));
y(1)= exp(-g_p*tau)*pr;% W
y(2)= u_c;

%% SYSTEM STRUCT
model.sym.x = x;
model.sym.xdot = xdot;
model.sym.p = p;
model.sym.x0 = [m0,p0,inp0];
model.sym.y = y;
model.sym.k=k;
