%> @breif makeIQM compiles the IQM model
%> Note this is part of a template created in the GTSBOX toolbox, 'xx' need to be replaced.
%> Make sure to delete the model files in the AMICI folder  before running
%> this. 

IQMdirectory=['..',filesep,'..',filesep,'..',filesep,'Dependencies',filesep,'IQMtools'];
addpath(genpath(IQMdirectory))


[exdir,~,~]=fileparts(which('Llamosi2016.txtbc'));

% change into right folder
OldFolder = cd;
cd(exdir);

% IQMmodel
model=IQMmodel('Llamosi2016.txtbc');
IQMmakeMEXmodel(model, 'MEX_Llamosi2016');

% change back to old foler
cd(OldFolder);