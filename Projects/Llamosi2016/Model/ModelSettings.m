%> @brief Model setting class, specific to a project
%> Note this is part of a template created in the GTSBOX toolbox, 'xx' need to be replaced.

classdef ModelSettings
   properties
      name = 'Llamosi2016' % name of the model
       
      %> fnHandle to simulate the model.  @simulate_Llamosi2016 is the function handle for AMICI
      fnHandle = @MEX_Llamosi2016 ;
      
       %> Model constants, the parameters values for which sensitivities are not calculated, refers to 'k' in the sym file of AMICI, vector
      constants = [1e-10,1e-10,10e-10,30,10,exp(3.4)] % initial conditions, t_in, k, tau

       %> Default values of parameters, vector
      paramNominal = [2.3000   -1.2200   -5.5200];
      
      %> Names of parameters, cell array with strings
      paramNames = {'kp','gm','gp'}
      
      %> Log transform parameter values, true or false, if this is true also the model sym file should have the corresponding option in AMICI specified
      tolog = false ;
      
      %> Name of observation, string
      obsNames={'Pr'};
      
     %> number of  states which need initial conditions
      nstates=3;
      
      %> Error model, cell array with strings for each observable, only 'additive' or 'multiplicative' is supported
      errorModel={'multiplicative'};
      
      %> Initial value for the error model, cell array with vectors, each corresponding to one observable
      errorModelinit={[50,0.1]};
      
      
      %> ODE model integration  tolerance, intege
      abstol=1e-3;
      rtol=1e-3;
      
       %> ODE model integration initial time point
       t0=0;

       %> parameter bounds used for estimation
      paramLowerbounds=[];
      paramUpperbounds=[];
   end
end
