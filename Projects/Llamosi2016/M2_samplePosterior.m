%> @brief  Samples the posterior for single-cell parameter estimates after obatining the first stage
%> @param  id a number indicating the dataset to be used
%> @retval [] the `singlecell_estimates` structure from the first stage is augmented with the results from sampling and saved in the same input file
function []=M2_samplePosterior()

% Inializes the relevant paths
addpath(genpath(['..',filesep,'..',filesep,'Dependencies',filesep,'Src',filesep]))
addpath(['..',filesep,'..',filesep,'Dependencies',filesep,'Tests',filesep])
addpath(genpath(['..',filesep,'..',filesep,'Dependencies',filesep,'IQMtools']))


% Check in which Dependencie folder PESTO is located
addpath(genpath(['..',filesep,'..',filesep,'Dependencies',filesep,'PESTO']))
addpath(genpath(['..',filesep,'..',filesep,'Dependencies',filesep,'mcmcstat']))

addpath(genpath(['.',filesep,'UserFns']))
addpath(genpath(['.',filesep,'Model']))  


% load the result file from the first stage: 
% Ensure you have the right file. 
fname=['./ResultsNLME/AR_pool_1_Result_Llamosi_Data_full_Llamosi2016'];
load(fname);

% initialise parpool, e.p. jobs, use an appropriate cluster
c = parcluster('local');
c.NumWorkers = 40;

parpool(c, c.NumWorkers);
nsamples=1e4; % or 1e5
singlecell_estimates=singlecell_estimates;
Model=Model;

for i=1:length(singlecell_estimates)
singlecell_estimates(i).Post_samples=[];
singlecell_estimates(i).Post_Mean=[];
singlecell_estimates(i).Post_Var=[];
end


% 
parfor i=1:length(singlecell_estimates)  
[parameters]=sampleSinglecellEstimates(singlecell_estimates(i),nsamples,Model);
singlecell_estimates(i).Post_samples=parameters;
singlecell_estimates(i).Post_Mean=mean(parameters.S.par(:,nsamples/2:end),2)';
singlecell_estimates(i).Post_Var=cov(parameters.S.par(:,nsamples/2:end)');
end
save(fname,'singlecell_estimates','-append')
