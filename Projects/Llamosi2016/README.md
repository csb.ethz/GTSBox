#### Project: Llamosi2016
## Data folder
The data to be used for estimation is in `DataForEstimation`.

## Model specification
In the `Model` folder, the txt file is converted to a mex function using IQM tools.

In the `Model/AMICIModel` folder  consists of a symbolic model file `_syms.m` which  is converted to `.mex` simulation files by AMICI using `makeAmiModel.m`.

The `ModelSettings.m` contains model specific settings that should be modified by the user according to the model.


## Estimation
1. Install IQM tools using the `installIQMtoolsInitial.m` when using this for the first time.
2. Run the matlab files from M1 to M4.

## Note
1. This has been checked on Windows and Linux system. No guarantee on MacOS.
2. To run AMICI model instead, add the relevant paths in the initPaths function.
3. Rather than using linearisation, we use sampling to estimate the single-cell uncertainty when using IQM tools.
