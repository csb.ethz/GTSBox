
#!/bin/bash
  mkdir "$1"
  cd "$1"
  mkdir Model
  cp ../templates/ModelSettings.m Model/ModelSettings.m
  cp ../templates/makeAmiModel.m  Model/makeAmiModel.m
  cp ../templates/initPaths.m  initPaths.m
  mkdir DataForEstimation
  mkdir RawData
  mkdir UserFns
  mkdir SubmitScripts
  touch SubmitScripts/submitFit"$1".sh
  mkdir AnalysisNoteBook
  touch README.md
  echo "#### $1" >> README.md
  touch Fit"$1".m
