%> @brief Model setting class, specific to a project
%> Note this is part of a template created in the GTSBOX toolbox, 'xx' need to be replaced.

classdef ModelSettings
   properties
   %> Name of the model, string
      name = 'xx'

   %> function handle to simulate model, function handle starting with "@.."
      fnHandle = @xx

   %> Model constants, the parameters values for which sensitivities are not calculated, refers to 'k' in the sym file of AMICI, vector
      constants = [];

  %> The parameter names for which sensitivities are not calculated, refers to 'k' in the sym file of AMICI, vector
         constants_name = {};

   %> Default values of parameters, vector
      paramNominal = [xx]

   %> Names of parameters, cell array with strings
      paramNames = {'xx','xx'}

   %> Log transform parameter values, true or false, if this is true also the model sym file should have the corresponding option in AMICI specified
      tolog = true

   %> Name of observation, string
      obsNames={'xx'};

   %> number of  states which need initial conditions
      nstates=xx;

   %> Error model, cell array with strings for each observable, only 'additive' or 'multiplicative' is supported
      errorModel={'xx'};

   %> Initial value for the error model, cell array with vectors, each corresponding to one observable
      errorModelinit={[xx]};

   %> ODE model integration absolute tolerance, integer
      abstol=1e-6;

   %> ODE model integration relative tolerance, integer
      rtol=1e-6;

   %>  ODE integration initial time point, integer
      t0=[xx];

   %> Bounds Parameter bounds for individual parameters, vector
      paramLowerbounds=[xx];

   %> Bounds Parameter bounds for individual parameters, vector
      paramUpperbounds=[xx];
   end
end
