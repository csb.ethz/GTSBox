%> @breif makeAmiModel compiles the AMICI model
%> Note this is part of a template created in the GTSBOX toolbox, 'xx' need to be replaced.


addpath(genpath(['..',filesep,'..',filesep,'..',filesep,'Dependencies',filesep,'AMICI']))
[exdir,~,~]=fileparts(which('xx_syms.m'));

% compile the model
% 1: indicates the second order will be computed...
amiwrap('xx_syms','xx_syms',exdir,1)
