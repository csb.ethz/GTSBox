#### Instruction to make Projects
This folder contains the models/ data sets from different projects.
All the estimation procedures for the projects should be called from their respective project folder.

Use the script `make-project.sh` to make such a project folder with appropriate names e.g. "test":

Example:

		make-project.sh test

Every project folder should be structured as follows:

1. Model: Folder with all the model files (required, model is read, simulated from this folder).
2. DataForEstimation: Data used for estimation (mandatory, data is accessed in this folder).
3. RawData: Raw data from an experiment/ publication and scripts for processing it.
4. ResultsNLME: Result folder after estimation where all results of the project are stored.
5. SubmitScripts: Submit scripts for SGE exclusive to that model
6. AnalysisNotebook: A jupyter notebook for analysis
7. UserFns: A folder where user defined functions should be defined for
	calling model simulation : `Src/Auxiliary/callSimulation.m` (default function used needs AMICI, and is in `Src/Auxiliary` folder)
	getting initial weights to be used for WOLS: `getWeights.m`
	calling custom function to approximate the Hessian/ FIM: `getHessianApprox.m`

If any of the functions above around in `UserFns`, then they will be used instead of the default functions found in the `Src/Auxiliary` folder.

Every `Model` folder should contain a script that describes some properties of the model:

		ModelSettings.m

and a script to convert the symbolic model to a `.mex` file:

		makeAmiModel.m

The `templates` folder has templates for both these files as well to guide you in filling the appropriate fields for your application.


In order to run the estimation, the user must write a script that passes relevant options to the `CallRunGTS.m` function with the script `_Fit__.m`. See `FitTransfection2018.m` or `M1_FitFS_Llamosi` for an example.

`SubmitScripts` folder contains shell scripts which can be used for submitting the jobs to an SGE cluster (and is guaranteed to work for the stelling cluster). This can be submitted by for example:

		qsub SubmitScripts/submitFitTransfectioModel.sh

The results from the estimation are `.mat` files in the `ResultsNLME` folder.


#### Data format used in estimation
The format of the data used for estimation is similar to that of Monolix's.
The typical column names are


|Column name| Description| Mandatory|
|:------------- | :----------: | -----------:|
|Time| Time points|_Yes_|
|Y|Observation value|_Yes_|
|TrackID|Track ID of the cell|_Yes_|
|YID| Observation ID i.e. the ID of the readout|_Yes_|
|PopulationID| Any covariate ID|No|
|LinID| Lineage ID |_No_ |
|GenID| Generation number| _No_ |
|TimDiv| Time at which the cell divides |_No_|

#### List of Projects:

|No. | Model name| Short description|Reference|
|:------------- | :----------: | -----------:|-----------:|
|1.| Transfection model| Study of mRNA translation dynamics upon transfection|[Fröhlich et al. ][1] |
|2.| Llamosi | _Hog1 model_ | [Dharmarajan et al.][2]|
|3.| _Mup1_| _Endocytosis model_|[Dharmarajan et al.][2]|

[1]: https://www.nature.com/articles/s41540-018-0079-7
[2]: https://www.cell.com/cell-systems/fulltext/S2405-4712(18)30481-2


Note: Models in italics will be added to the repo.

###### References
1. Fröhlich, F., Reiser, A., Fink, L. et al. Multi-experiment nonlinear mixed effect modeling of single-cell translation kinetics after transfection. npj Syst Biol Appl 4, 42 (2018). https://doi.org/10.1038/s41540-018-0079-7
2. Dharmarajan, Lekshmi, et al. "A simple and flexible computational framework for inferring sources of heterogeneity from single-cell dynamics." Cell systems 8.1 (2019): 15-26. https://doi.org/10.1016/j.cels.2018.12.007
