%% Make AMICI model
addpath(genpath(['..',filesep,'..',filesep,'..',filesep,'Dependencies',filesep,'AMICI']))
[exdir,~,~]=fileparts(which('Transfection2018_log_syms.m'));

% compile the model
% 1: indicates the second order will be computed...
amiwrap('Transfection2018_log_syms','Transfection2018_log_syms',exdir,1)
