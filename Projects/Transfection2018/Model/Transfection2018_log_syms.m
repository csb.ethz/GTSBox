function [model] = Transfection2018_log_syms()

%% CVODES OPTIONS
model.param = 'log';

%% STATES

syms MRNA GFP 

x = [MRNA,GFP];

%% PARAMETERS

syms  delta1 pbeta k2_m0_scale t0 offset

p = [delta1,pbeta,k2_m0_scale,t0,offset];

%% INPUT 

u = sym.empty(0,0);

%% SYSTEM EQUATIONS
syms t

xdot = sym(zeros(size(x)));

xdot(1) = - delta1*MRNA + dirac(t-t0);% 1/(st*sqrt(2*pi))*exp(-(t-t0)^2/(2*st^2));
xdot(2) = + k2_m0_scale*MRNA - pbeta*GFP;


%

%% INITIAL CONDITIONS

x0 = sym(zeros(2,1));

x0(1) = 0;

%% OBSERVABLES

y = sym(zeros(1,1));

y(1) = log(GFP + offset);

%% SYSTEM STRUCT

model.sym.x = x;
model.sym.k = [];
model.sym.u = u;
model.sym.xdot = xdot;
model.sym.p = p;
model.sym.x0 = x0;
model.sym.y = y;
end