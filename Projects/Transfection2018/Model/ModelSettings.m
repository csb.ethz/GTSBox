%> @brief Model setting class, specific to a project
%> Note this is part of a template created in the GTSBOX toolbox.

classdef ModelSettings
   properties

      %> Name of the model, string
      name = 'Transfection2018'


      %> function handle to simulate model, function handle starting with "@.."
      fnHandle = @simulate_Transfection2018_log_syms


      %> The parameters values for which sensitivities are not calculated, vector
      constants = [];

     %> The parameter names for which sensitivities are not calculated, vector
      constants_name = [];

      %> Default values of parameters, vector
      paramNominal =10.^([-1,-1,3,0,1])


      %> Names of parameters, cell array with strings
      paramNames = {'delta1' 'pbeta' 'k2_m0_scale' 't0' 'offset'}


      %> Log transform parameter values, true or false
         tolog = true

      %> Name of observation, string
         obsNames={'GFP'};

      %> number of  states which need initial conditions
         nstates=2;

      %> Error model, cell array with strings
         errorModel={'additive'};

      %> Initial value for the error model, cell array with vectors
         errorModelinit={[0.3]};


      %> ODE model integration absolute tolerance, integer
         abstol=1e-6;

      %> ODE model integration relative tolerance, integer
         rtol=1e-6;

      %>  ODE integration initial time point, integer
         t0=[0];

      %> Bounds Parameter bounds for individual parameters, vector
         paramLowerbounds=[-10,-10,-10,-10,-10];

      %> Bounds Parameter bounds for individual parameters, vector
         paramUpperbounds=[10,10,10,10,10];
   end
end
