%
% @brief SampleSinglecellEstimates samples the parameters of a single cell using PESTO
% toolbox 
% @params singlecell_estimates structure of single cell estimates of one
% cell
% @params nsamples number of samples
% @retva; singlecell_estimates the structre is appended with the
% distribution and sampling results

function [parameters]=sampleSinglecellEstimates(singlecell_estimates,nsamples,Model)

% uncomment this part if running stand alone

% addpath(genpath('../../Dependencies'))
% addpath(genpath('./UserFns'))
% addpath(genpath('../../Src/NLME/'))
% addpath(genpath('./'))

% load results from the first stage
% load('./ResultsNLME/AR_pool_1_Result_Transfection2018_Data_deGFP.mat')

% Seed random number generator
rng(0);


% data
data_cell=singlecell_estimates.dat;
texp=singlecell_estimates.t;
ind_est=singlecell_estimates.params;
Hessian_est=singlecell_estimates.Hessian;
int_opts=singlecell_estimates.int_opts;
sigma_y=singlecell_estimates.measurement_sd;
data_cell.time=texp;



%% Generation of the structs and options for PESTO

% parameters
% display(' Prepare structs and options...')
parameters.name   = Model.indParams.namePars;
parameters.min    = Model.indParams.lowerbounds;
parameters.max    = Model.indParams.upperbounds;
parameters.number = length(parameters.name);

% objective function
objectiveFunction = @(para) 2*(sampleWOLS(data_cell.time,para,sigma_y,[],data_cell,int_opts));

% PestoOptions
optionsPesto           = PestoOptions();
optionsPesto.obj_type  = 'log-posterior';
optionsPesto.comp_type = 'sequential';

optionsPesto.plot_options.add_points.par = ind_est;
optionsPesto.plot_options.add_points.logPost = objectiveFunction(ind_est);

%% Parameter Sampling
% Covering all sampling options in one struct
% display(' Sampling without prior information...');
optionsPesto.MCMC.nIterations  = nsamples;

% PT (with only 1 chain -> AM) specific options:
optionsPesto.MCMC.samplingAlgorithm = 'PT';
optionsPesto.MCMC.PT.nTemps         = length(ind_est);
optionsPesto.MCMC.PT.exponentT      = 6;
optionsPesto.MCMC.PT.regFactor      = 1e-8;

% Initialize the chains by choosing an initial point and a 'large'
% covariance matrix
optionsPesto.MCMC.theta0 = ind_est';
optionsPesto.MCMC.sigma0 = 1e5 * eye(length(ind_est));

% Run the sampling
parameters = getParameterSamples(parameters, objectiveFunction, optionsPesto);

% Use a diagnosis tool to see, how plotting worked (see burn-in etc.)
% plotMCMCdiagnosis(parameters, 'parameters');

% %% Calculate Confidence Intervals
% Confidence Intervals for the Parameters are inferred from the local
% optimization and the sampling information.

% Set confidence levels
confLevels = [0.8, 0.9, 0.95, 0.99];

% display(' Computing confidence intervals...');
parameters = getParameterConfidenceIntervals(parameters, confLevels, optionsPesto);

end 
%% Perform another sampling
% optionsPesto.MCMC.theta0 = Model.indParams.inits';
% optionsPesto.MCMC.sigma0 = 1e5 * eye(length(ind_est));
% parametersNew =parameters; % or another starting value 
% parametersNew = getParameterSamples(parametersNew, objectiveFunction, optionsPesto);

%% 



