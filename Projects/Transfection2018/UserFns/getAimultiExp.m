%> @brief get_ai_multiExp gets the design matrix for each cell given the covarite flag they have
%> @param covariate_flag covariate flag for each cell, a vector 
%> @param nbeta number of  elements in the mean vector  
%> @retval A 3-D array of design matrices for the fixed effects, where for  each matrix, the columns are nbeta and the nrow are the number of elements in each class

function Ai=getAimultiExp(nbeta,covariate_flag)

id=find(covariate_flag==1);
if id==1    
tmp=eye(nbeta-1,nbeta);
Ai=[tmp(:,1),tmp(:,6),tmp(:,2:5)];

else
 tmp=eye(nbeta-1,nbeta);
 Ai=[tmp(:,6),tmp(:,1:2),tmp(:,3:5)];
end
end