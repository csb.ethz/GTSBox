%> @brief SSObjectiveFn is a user specified objective which is specific to the project. This can be passed to the optimizer through the option 'fnHandle' in the second stage.
%> @param parameters A vector of parameters
%> @param invCi  Inverse of the uncertainty of the the cell-specific parameter estimates
%> @param Ai     3D array of the design matrices for the fixed effects
%> @param singlecell_estimates Estimates for the single cells, a matrix
%> @param varargin  User specific


function [obj_fn]=SSObjectiveFn(parameters,invCi,Ci,Ai,singlecell_estimates,varargin)

SSobjOpts=varargin{1};
covariate_flag=SSobjOpts.covariate_flags;

[betahat,D]=getParameterMatFn(parameters);
[p1]=cholcov(D(:,:,1));
[p2]=cholcov(D(:,:,2));

if ~any([isempty(p1),isempty(p2)])

    N=length(singlecell_estimates);

    % covariate_flags converted to dummy variable coding
    ncov=length(unique(covariate_flag));
    covariate_flags=generateDummyVar(covariate_flag,ncov);

    ncells=length(invCi);

    t1=0;
    t2=0;
    for i=1:ncells
        for k=1:ncov
            % Note: The choice of modelling the covariance this way is because
            % often we may want to impose some constraints on the covariance /
            % mean matrix. We could consider using a matrix to left and right
            % multiply a D matrix as well. But currently this is not
            % implemented.
            t1=t1+covariate_flags(i,k)*logdet((Ci(:,:,i)+D(:,:,k)));
            t2=t2+covariate_flags(i,k)*(singlecell_estimates(i,:)'-...
                Ai(:,:,i)*betahat')'*getStableInverse(Ci(:,:,i)+...
                D(:,:,k))*(singlecell_estimates(i,:)'-Ai(:,:,i)*betahat');
        end
    end

    obj_fn=t1+t2;

    if ~isreal(obj_fn)
        obj_fn=10^10;
    else
        obj_fn=obj_fn;
    end
else
    obj_fn=10^10;
end

end
