%%%------------------------------------------------------------------------
%> @brief Function that takes in a vector of parameters and converts it to
%the mean and covariance matrix used in second stage. 
%> @param parameters A vector of parameters
%> @retval betahat  The mean of cell-specific parameters, a vector
%> @retval D The covariance of cell-specific parameters, a matrix or array
function [betahat,D]=getParameterMatFn(parameters,varargin)


betahat=reshape(parameters(1:6),1,6);
D=ones(length(betahat)-1,length(betahat)-1,2);

D(:,:,1)=diag(exp([parameters(7),parameters(8),...
parameters(10),parameters(11),parameters(12)])).^2;

D(:,:,2)=diag(exp([parameters(7),parameters(9),...
parameters(10),parameters(11),parameters(12)])).^2;
end
