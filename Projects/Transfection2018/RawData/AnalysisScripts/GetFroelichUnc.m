addpath(genpath('./MEMOIR'))
load('/Users/dlekshmi/Desktop/Repos/MeMix/Modelling/ResultsNLME/AR_Result_Transfection2018_Data_deGFP.mat')
addpath(genpath('../../src/NLME'))

load Model_Data_transfection_norm.mat
load results_norm.mat
load norm_SCTL_fits.mat

residual_eGFP_mem = NaN(size(Data{1}.SCTL.Y));
beta = Model.exp{1}.beta(parameters_MEM.MS.par(:,1));
t = Data{1}.SCTL.time;
optionmu=struct();
optionmu.rtol=1e-12;
optionmu.atol=1e-12;
int_opts=GLS_params(1).int_opts;
int_opts.sensi=2;
beta = Model.exp{1}.beta(parameters_MEM.MS.par(:,1));
t = Data{1}.SCTL.time;

for icl = 1:size(Data{1}.SCTL.Y,3)
    sol = Model.exp{1}.model(t,P{1}.SCTL.bhat(:,icl) + beta,[],optionmu);
    data.Y=Data{1}.SCTL.Y(:,:,icl);
    data.YID=repmat(1,size(data.Y));
    thetahat={[sqrt(nanmean((data.Y-sol.y).^2))]};
    p0=log(10.^(P{1}.SCTL.bhat(:,icl) + beta));
    [~,~,~,~,FIM2(:,:,icl),FIM(:,:,icl)]=callSimulation(t,p0,data,thetahat,int_opts,{'additive'});
    InFIM(:,:,icl)=getStableInverse(FIM2(:,:,icl));
end

% For the second experiment
beta = Model.exp{2}.beta(parameters_MEM.MS.par(:,1));
for icl = 1:size(Data{2}.SCTL.Y,3)
    sol = Model.exp{2}.model(t,P{2}.SCTL.bhat(:,icl) + beta,[],optionmu);
    data.Y=Data{2}.SCTL.Y(:,:,icl);
    data.YID=repmat(1,size(data.Y));
    thetahat={[sqrt(nanmean((data.Y-sol.y).^2))]};
    p0=log(10.^(P{2}.SCTL.bhat(:,icl) + beta));
    [~,~,~,~,FIM2(:,:,icl+236),FIM(:,:,icl+236)]=callSimulation(t,p0,data,thetahat,int_opts,{'additive'});
    InFIM(:,:,icl+236)=getStableInverse(FIM2(:,:,icl+236));
end

save('Froelich_uncertainities.mat','InFIM','FIM2','FIM')
