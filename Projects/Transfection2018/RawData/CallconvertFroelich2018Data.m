% Call the script after adding the code/ data from https://zenodo.org/record/1228899. into a folder
% called Froelich2018
% Convert the data from Froelich 2018 to something usable by me.
% Only taking the data tha they used for estimation.
% Saves everything in DataForEstimation/ folder

addpath(genpath('..',filesep,'DataForEstimation',filesep))
load('.',filesep,'Froelich2018',filesep,'Model_Data_transfection_norm.mat')
exp_names={'eGFP','deGFP'};
for i=1:2
Exp1=Data{i}.SCTL;
data_exp1=convertForelich2018Data(Exp1,i);
writetable(data_exp1,['..',filesep,'DataForEstimation',filesep,'Data_', exp_names{i},'.csv'])
end
