%% Convert data from Froelich et al to format
%  usable by me and monolix:

function [data_SCTL] = convertForelich2018Data(expid,id)
% Col names: Time, Y, CellID, YID
data_SCTL=[];

for i = 1:size(expid.Y,3)
    tmp_data=cat(2,expid.time,expid.Y(:,:,i),...
        repmat(i,size(expid.time)),...
        repmat(1,size(expid.time)),...
        repmat(id,size(expid.time)));
    data_SCTL=cat(1,data_SCTL,tmp_data);
end
   data_SCTL=array2table(data_SCTL,'VariableNames',... 
       {'Time','Y','TrackID','YID','PopulationID'}); 
   data_SCTL.AMT(data_SCTL.Time==0)=1;
   data_SCTL.AMT=repmat(0,length(data_SCTL.Time),1);
   data_SCTL.AMT(data_SCTL.Time==0)=1;
 
end