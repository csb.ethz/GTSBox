#### Project: Transfection2018
## Data folder
The data to be used for estimation is in `DataForEstimation`. The original data was obtained from https://zenodo.org/record/1228899, and was converted to the  `.csv` files using the script `CallconvertFroelich2018Data.m` in `RawData` folder. In this study two experiments were done, therefore we have two different data files: 'Data_eGFP.csv' and 'Data_deGFP.csv'.

## Model specification
In the `Model` folder  consists of a symbolic model file `_syms.m` which  is converted to `.mex` simulation files by AMICI using `makeAmiModel.m`. The `ModelSettings.m` contains model specific settings that should be modified by the user according to the model. Refer to the doc page on `ModelSettings` class.

## Estimation
After compiling the model with the `makeAmiModel` script, we start with the estimation using the `FitTransfection2018.m` script. We begin by initializing the paths with


    initPaths()


Then, we need to initialize the options for the GTS and run the estimation

    % get the GTS options: a default structure
    GTSoptions=getGTSoptions();

### Estimation of the first stage
We can modify options for the first stage as follows.

    GTSoptions.FSopt.gloalgMaxeval=1e4; % optimization for the initial estimates.
    GTSoptions.FSopt.runtype=1;         % type of first stage estimation
    GTSoptions.compileflag=0;           % do not compile the model
    GTSoptions.FSopt.parWorkers=50;     % number of threads to be used by parpool using 'local' cluster profile.



The estimation is then called using the `CallRunGTS.m` function, by passing options to the first stage  estimation through the options structure.

	% 1. Run the first stage estimation
	GTSoptions.FSopt.GLS.errorEstimation='PL';

	% 1a. With PL-pool
	CallRunGTS('Transfection2018',[], 'Data_eGFP',GTSoptions)
	CallRunGTS('Transfection2018',[], 'Data_deGFP',GTSoptions)

	% 1b. With AR-pool 
	GTSoptions.FSopt.GLS.errorEstimation='AR';
	CallRunGTS('Transfection2018',[], 'Data_eGFP',GTSoptions)
	CallRunGTS('Transfection2018',[], 'Data_deGFP',GTSoptions)


### Estimation of the second stage
We then run the second stage and using different options. The second stage can be optimized with either EM or with an optimizer (Nlopt in our case).


    % Second stage and using different options
    GTSoptions.FSopt.runtype=0;             % Do not run the first stage
    GTSoptions.runSecondStage=1;            % Will run second stage

    GTSoptions.SSopt.inpfiles={'Data_eGFP','Data_deGFP'};  % Passing more  1 input files..



#### Estimation with an optimizer
We can estimate a model for each experiment separately, without initializing the mean and covariance parameters. In this case, the empirical mean and covariance from the first stage estimates are taken.


    % 2: Do not initialize the parameters, and treat two experiments separate
    % from one another
    GTSoptions.SSopt.Optimization='Optimizer';  % Use with an optimizer
    GTSoptions.SSopt.jointExps=0;               % Do not joins datasets together
    GTSoptions.SSopt.initialbeta0=[];
    GTSoptions.SSopt.initialD0=[];
    GTSoptions.SSopt.fname_append='Ex1_';       % Result file is appended with the string Ex_1
    CallRunGTS('Transfection2018', [],[],GTSoptions);


We can add the bounds for the parameter to be estimated:

    % 3. With specification of the bounds for the optimizer
    GTSoptions.SSopt.initialbeta0=[];
    GTSoptions.SSopt.initialD0=[];
    GTSoptions.SSopt.optim.lower_bounds=[repmat(-10,1,10),repmat(-1,1,10)];
    GTSoptions.SSopt.optim.upper_bounds=[repmat(10,1,10),repmat(1,1,10)];
    GTSoptions.SSopt.fname_append='Ex2_';
    CallRunGTS('Transfection2018', [],[],GTSoptions)


We can initialize the parameters and estimate a common mean and covariance for the both the datasets:

    % 4. With initialization of the parameters
    GTSoptions.SSopt.jointExps=1;
    GTSoptions.SSopt.initialbeta0=[-2   -2.7    5.    0.1037    2.0328];
    GTSoptions.SSopt.initialD0=eye(length(GTSoptions.SSopt.initialbeta0));
    GTSoptions.SSopt.optim.lower_bounds=[];
    GTSoptions.SSopt.optim.upper_bounds=[];
    GTSoptions.SSopt.fname_append='Ex3_';
    CallRunGTS('Transfection2018', [],[],GTSoptions)


We can also provide a user specific function to carry out the optimization. We provided our own objective function, such that we can estimate some variance and mean parameters to be the same in the experiments.

    % 5. With a user specified fnhandle and covariate mapping function
    GTSoptions.SSopt.fname_append='Ex4_';
    GTSoptions.SSopt.jointExps=1;
    GTSoptions.SSopt.covariate_flag=[zeros(1,236),ones(1,394)];
    GTSoptions.SSopt.initialbeta0=[-2  -2.4   -2.7    5.    0.1037    2.0328];
    GTSoptions.SSopt.initialD0=eye(length(GTSoptions.SSopt.initialbeta0));
    GTSoptions.SSopt.optim.fnHandle=@SSObjectiveFn;   % user defined objective function
    GTSoptions.SSopt.optim.fnParamMapper=@getParameterMatFn;% user defined function that maps parameter vectors to the form used in the objective function
    GTSoptions.SSopt.CovariateFn=@getAimultiExp; % user defined function handle that returns the design matrix for each cell
    CallRunGTS('Transfection2018', [],[],GTSoptions)


We can also provide a user-defined function to carry out the optimization. We provided our own objective function, such that we can estimate some variance and mean parameters to be the same in the experiments; a mapping function that converts vectors of parameters to the mean and covariance structures.

#### Estimation with EM
We can also estimate using the EM by changing the Optimization option.


    % 6. With out covariates for the fixed effects, treating each experiment separate
    GTSoptions=getGTSoptions();
    GTSoptions.FSopt.runtype=0;
    GTSoptions.compileflag=0;
    GTSoptions.runSecondStage=1;           % Will run sec stage
    GTSoptions.SSopt.inpfiles={'Data_eGFP','Data_deGFP'};  % Passing more than 1 input files..

    GTSoptions.SSopt.Optimization='LGTS_EM';
    GTSoptions.SSopt.jointExps=0;
    GTSoptions.SSopt.fname_append='Ex5_';
    CallRunGTS('Transfection2018', [],[],GTSoptions)


We can again estimate a common mean and covariance for both the datasets with EM with and without initializing the mean and covariance.

    % 7. Treating both experiments together
    GTSoptions.SSopt.jointExps=1;
    GTSoptions.SSopt.fname_append='Ex6_';
    CallRunGTS('Transfection2018', [],[],GTSoptions)


We can again estimate a common mean and covariance for both the datasets with EM with and without initializing the mean and covariance.

    % 8. With initialization, and treating both experiments together
    GTSoptions.SSopt.initialbeta0=[-2    -2.7    5.    0.1037    2.0328];
    GTSoptions.SSopt.initialD0=eye(length(GTSoptions.SSopt.initialbeta0));
    GTSoptions.SSopt.fname_append='Ex7_';
    CallRunGTS('Transfection2018', [],[],GTSoptions)


We can also provide a function that generates a design matrix for the fixed effects with `GTSoptions.SSopt.CovariateFn`.

    % 9. With initialization, and covariate on the fixed terms
    GTSoptions.SSopt.initialbeta0=[-2  -2.4   -2.7    5.    0.1037    2.0328];
    GTSoptions.SSopt.CovariateFn=@getAimultiExp;
    GTSoptions.SSopt.fname_append='Ex8_';
    CallRunGTS('Transfection2018', [],[],GTSoptions);


## Analysis Notebook folder
All analysis is done in the `Jupyter` notebook in the `AnalysisNotebook` folder.
