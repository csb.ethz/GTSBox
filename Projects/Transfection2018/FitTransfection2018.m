%
% @brief FitTransfection2018 fits the Transfection dataset with the GTS. An
% example script.
%


function []= FitTransfection2018()

initPaths()

% get the GTS options: a default structure
GTSoptions=getGTSoptions();
GTSoptions.FSopt.gloalgMaxeval=1e4;
GTSoptions.FSopt.runtype=1;
GTSoptions.compileflag=0;
GTSoptions.FSopt.parWorkers=50;

% 1. Run the first stage estimation
% GTSoptions.FSopt.GLS.errorEstimation='PL';
% 
% % 1a. With PL-pool
% CallRunGTS('Transfection2018',[], 'Data_eGFP',GTSoptions)
% CallRunGTS('Transfection2018',[], 'Data_deGFP',GTSoptions)

% 1b. With AR-pool 
GTSoptions.FSopt.GLS.errorEstimation='AR';
CallRunGTS('Transfection2018',[], 'Data_eGFP',GTSoptions)
CallRunGTS('Transfection2018',[], 'Data_deGFP',GTSoptions)

%%
% Second stage and using different options
GTSoptions.FSopt.runtype=0;
GTSoptions.compileflag=0;
GTSoptions.runSecondStage=1;

GTSoptions.SSopt.Optimization='Optimizer';
GTSoptions.SSopt.inpfiles={'Data_eGFP','Data_deGFP'};
GTSoptions.SSopt.ncluster=1;
GTSoptions.SSopt.jointExps=0;


% 2: Do not initialize the parameters, and treat two experiments separate
% from one another
GTSoptions.SSopt.initialbeta0=[];
GTSoptions.SSopt.initialD0=[];
GTSoptions.SSopt.fname_append='Ex1_';
CallRunGTS('Transfection2018', [],[],GTSoptions)

% 3. With specification of the bounds for the optimizer
GTSoptions.SSopt.initialbeta0=[];
GTSoptions.SSopt.initialD0=[];
GTSoptions.SSopt.optim.lower_bounds=[repmat(-10,1,10),repmat(-1,1,10)];
GTSoptions.SSopt.optim.upper_bounds=[repmat(10,1,10),repmat(1,1,10)];
GTSoptions.SSopt.fname_append='Ex2_';
CallRunGTS('Transfection2018', [],[],GTSoptions)

% 4. With initialization of the parameters
GTSoptions.SSopt.jointExps=1;
GTSoptions.SSopt.initialbeta0=[-2   -2.7    5.    0.1037    2.0328];
GTSoptions.SSopt.initialD0=eye(length(GTSoptions.SSopt.initialbeta0));
GTSoptions.SSopt.optim.lower_bounds=[];
GTSoptions.SSopt.optim.upper_bounds=[];
GTSoptions.SSopt.fname_append='Ex3_';
CallRunGTS('Transfection2018', [],[],GTSoptions)


% 5. With a user specified fn handle and covariate mapping function
GTSoptions.SSopt.fname_append='Ex4_';
GTSoptions.SSopt.jointExps=1;
GTSoptions.SSopt.covariate_flag=[zeros(1,236),ones(1,394)];
GTSoptions.SSopt.initialbeta0=[-2  -2.4   -2.7    5.    0.1037    2.0328];
GTSoptions.SSopt.initialD0=eye(length(GTSoptions.SSopt.initialbeta0));
GTSoptions.SSopt.optim.fnHandle=@SSObjectiveFn;
GTSoptions.SSopt.optim.fnParamMapper=@getParameterMatFn;
GTSoptions.SSopt.CovariateFn=@getAimultiExp;
CallRunGTS('Transfection2018', [],[],GTSoptions)

%% Example: Using the EM
% 6. With out covariates, treating each experiment separate
GTSoptions=getGTSoptions();
GTSoptions.FSopt.runtype=0;
GTSoptions.compileflag=0;
GTSoptions.runSecondStage=1;           % Will run second stage
GTSoptions.SSopt.inpfiles={'Data_eGFP','Data_deGFP'};  % Passing more than 1 input files..

GTSoptions.SSopt.Optimization='LGTS_EM';
GTSoptions.SSopt.jointExps=0;
GTSoptions.SSopt.fname_append='Ex5_';
CallRunGTS('Transfection2018', [],[],GTSoptions)

% 7. Treating both experiments together
GTSoptions.SSopt.jointExps=1;
GTSoptions.SSopt.fname_append='Ex6_';
CallRunGTS('Transfection2018', [],[],GTSoptions)

% 8. With initialization, and treating both experiments together
GTSoptions.SSopt.initialbeta0=[-2    -2.7    5.    0.1037    2.0328];
GTSoptions.SSopt.initialD0=eye(length(GTSoptions.SSopt.initialbeta0));
GTSoptions.SSopt.fname_append='Ex7_';
CallRunGTS('Transfection2018', [],[],GTSoptions)

% 9. With initialization, and covariate on the fixed terms
GTSoptions.SSopt.initialbeta0=[-2  -2.4   -2.7    5.    0.1037    2.0328];
GTSoptions.SSopt.CovariateFn=@getAimultiExp;
GTSoptions.SSopt.covariate_flag=[zeros(1,236),ones(1,394)];
GTSoptions.SSopt.fname_append='Ex8_';
CallRunGTS('Transfection2018', [],[],GTSoptions)

end

