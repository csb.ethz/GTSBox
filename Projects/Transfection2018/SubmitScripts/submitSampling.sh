#!/bin/bash
#$ -V
#$ -q sc03.q
#$ -cwd
#$ -pe openmpi624 50
#$ -N logs_TransfectionModel-sample
LD_PRELOAD=/usr/local/bsse/gcc/6.5/lib64/libstdc++.so.6 matlab -nosplash  -r "samplePosterior(); exit"
