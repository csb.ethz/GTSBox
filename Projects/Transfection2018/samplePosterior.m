function []=samplePosterior()
initPaths()

% change the result file accordingly
fname='./ResultsNLME/AR_pool_1_Result_Transfection2018_Data_deGFP.mat';
load(fname);

% initialise parpool, e.p. jobs, use an appropriate cluster
c = parcluster('local');
c.NumWorkers = 50;
parpool(c, c.NumWorkers);
nsamples=1e4;
singlecell_estimates=singlecell_estimates;
Model=Model;

for i=1:length(singlecell_estimates)
singlecell_estimates(i).Post_samples=[];
singlecell_estimates(i).Post_Mean=[];
singlecell_estimates(i).Post_Var=[];
end

parfor i=1:length(singlecell_estimates)
[parameters]=sampleSinglecellEstimates(singlecell_estimates(i),nsamples,Model);
singlecell_estimates(i).Post_samples=parameters;
singlecell_estimates(i).Post_Mean=mean(parameters.S.par(:,nsamples/2:end),2)';
singlecell_estimates(i).Post_Var=cov(parameters.S.par(:,nsamples/2:end)');
end
save(fname,'singlecell_estimates','-append')
